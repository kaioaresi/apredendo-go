package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func home(c echo.Context) error {
	msg := "Home"
	return c.String(http.StatusOK, msg)
}

func cadastro(c echo.Context) error {
	msg := "Cadastro"
	return c.String(http.StatusOK, msg)
}

func main() {
	e := echo.New()

	// Declaração de rotas
	e.GET("/", home)
	e.GET("/cadastro", cadastro)

	e.Logger.Fatal(e.Start(":8080"))
}

module teste-confluence

go 1.19

require (
	github.com/kentaro-m/blackfriday-confluence v0.0.0-20220126124413-8e85477b49b3
	github.com/russross/blackfriday/v2 v2.1.0
)

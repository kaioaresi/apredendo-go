package main

import "fmt"

func sender(ch chan<- string, msg string) {
	for i := 0; i < 5; i++ {
		ch <- msg
	}
	close(ch)
}

func main() {
	ch := make(chan string)

	var msg string

	fmt.Scanln(&msg)

	fmt.Println("Inicio programa!")

	go sender(ch, msg)

	for msg := range ch {
		fmt.Println(msg)
	}
}

package main

import "fmt"

type Animal struct {
	Patas int
	Som   string
}

func (a *Animal) Dog() string {
	return fmt.Sprintf("Patas: %d Som: %s", a.Patas, a.Som)
}

func (a *Animal) Cat() string {
	return fmt.Sprintf("Patas: %d Som: %s", a.Patas, a.Som)
}

func main() {
	dog1 := Animal{4, "Au! Au!"}
	cat1 := Animal{4, "Miau! Miau!"}
	fmt.Println(dog1.Dog())
	fmt.Println(cat1.Cat())
}

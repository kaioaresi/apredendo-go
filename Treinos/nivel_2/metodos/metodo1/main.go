package main

import "fmt"

type Carro struct {
	Modelo     string
	Fabricante string
	Ano        int
}

type Moto struct {
	Modelo     string
	Fabricante string
	Ano        int
}

type Pessoa struct {
	Nome    string
	Idade   int
	Veiculo Veiculo
}

// Definindo qual a assinatura para que seja considerado um veiculo é ter o metodo `buzina`
type Veiculo interface {
	buzina()
}

// Metodo carro
func (c Carro) buzina() {
	fmt.Println("Carro", c.Modelo, "Buzinou e saíu queimando pnel!")
}

// Metodo moto
func (m Moto) buzina() {
	fmt.Println("Moto", m.Fabricante, "Buzinou empinou!!!")
}

func main() {
	c1 := Carro{
		Modelo:     "HR-V",
		Fabricante: "Honda",
		Ano:        2022,
	}
	m1 := Moto{
		Modelo:     "CG 125",
		Fabricante: "Honda",
		Ano:        2022,
	}

	zed := Pessoa{
		Nome:    "Zed",
		Idade:   175,
		Veiculo: c1,
	}

	shen := Pessoa{
		Nome:    "Shen",
		Idade:   210,
		Veiculo: m1,
	}

	zed.Veiculo.buzina()
	shen.Veiculo.buzina()
}

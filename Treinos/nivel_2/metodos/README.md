# Metodos

Metodo simples

```go
package main

import "fmt"

type Carro struct {
	Modelo     string
	Fabricante string
	Ano        int
}

type Pessoa struct {
	Nome  string
	Idade int
	Carro
}

func (p *Pessoa) DadosCarro() {
	p.Nome = "Shen"
	fmt.Println(p.Carro.Fabricante, p.Carro.Ano, p.Carro.Modelo)
}

func main() {
	zed := Pessoa{
		Nome:  "Zed",
		Idade: 175,
		Carro: Carro{
			Modelo:     "HR-V",
			Fabricante: "Honda",
			Ano:        2022,
		},
	}

	zed.DadosCarro()
	fmt.Println(zed.Nome)
}
```

Criando interface para conectar assinatura de objetos

```go
type Carro struct {
	Modelo     string
	Fabricante string
	Ano        int
}

type Moto struct {
	Modelo     string
	Fabricante string
	Ano        int
}

type Pessoa struct {
	Nome    string
	Idade   int
	Veiculo Veiculo
}

// Definindo qual a assinatura para que seja considerado um veiculo é ter o metodo `buzina`
type Veiculo interface {
	buzina()
}

// Metodo carro
func (c Carro) buzina() {
	fmt.Println("Carro", c.Modelo, "Buzinou e saíu queimando pnel!")
}

// Metodo moto
func (m Moto) buzina() {
	fmt.Println("Moto", m.Fabricante, "Buzinou empinou!!!")
}

func main() {
	c1 := Carro{
		Modelo:     "HR-V",
		Fabricante: "Honda",
		Ano:        2022,
	}
	m1 := Moto{
		Modelo:     "CG 125",
		Fabricante: "Honda",
		Ano:        2022,
	}

	zed := Pessoa{
		Nome:    "Zed",
		Idade:   175,
		Veiculo: c1,
	}

	shen := Pessoa{
		Nome:    "Shen",
		Idade:   210,
		Veiculo: m1,
	}

	zed.Veiculo.buzina()
	shen.Veiculo.buzina()
}
```

Exeplo dois

```
type Animal struct {
	Patas int
	Som   string
}

func (a *Animal) Dog() string {
	return fmt.Sprintf("Patas: %d Som: %s", a.Patas, a.Som)
}

func (a *Animal) Cat() string {
	return fmt.Sprintf("Patas: %d Som: %s", a.Patas, a.Som)
}

func main() {
	dog1 := Animal{4, "Au! Au!"}
	cat1 := Animal{4, "Miau! Miau!"}
	fmt.Println(dog1.Dog())
	fmt.Println(cat1.Cat())
}
```
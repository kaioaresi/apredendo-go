package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Printf("\r[")
	for j := 0; j < 20; j++ {
		fmt.Print("#")
		time.Sleep(40 * time.Millisecond)
	}
	fmt.Print("]\n")
}

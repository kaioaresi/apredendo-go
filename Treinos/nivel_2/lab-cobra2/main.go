package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "TESTE",
	Short: "Descrição curta",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Comando")
	},
}

func main() {

	cobra.CheckErr(rootCmd.Execute())
}

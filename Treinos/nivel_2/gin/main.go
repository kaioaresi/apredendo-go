package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func home(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"msg": "Home",
	})
}

func cadastro(c *gin.Context) {
	c.String(http.StatusOK, "Cadastro")
}

func main() {

	r := gin.Default()

	// Cria rotas
	r.GET("/", home)
	r.GET("/cadastro", cadastro)

	r.Run() // default port ::8088
}

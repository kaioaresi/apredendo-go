package main

import "fmt"

func main() {
	// Criando um canal do tipo string com buffer de 1 mensage apenas
	menssage := make(chan string, 1)

	// Enviando uma mensagem para o canal
	menssage <- "Texto qualquer"

	// Recebendo a mensage no canal
	msg := <-menssage

	fmt.Println(msg)
}

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

// HandleRequest - teste
func HandleRequest() {
	http.HandleFunc("/", Home)
	http.HandleFunc("/cadastro", Cadastro)
	http.HandleFunc("/health", Healthcheck)
	fmt.Println("Iniciando servidor :8080....")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// Home - Teste
func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Home page")
	w.WriteHeader(http.StatusNoContent)
}

// Cadastro - tete
func Cadastro(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Cadastro user ", os.Getenv("NOME"))
	w.WriteHeader(http.StatusNoContent)
}

// Healthcheck - tete
func Healthcheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Healthcheck ok!")
	w.WriteHeader(http.StatusNoContent)
}

func main() {
	fmt.Println("Test-k api rest nível 1")
	HandleRequest()
}

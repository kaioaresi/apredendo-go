package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	tamanhoBar = 50
)

// loadingBar - barra de progresso
func loadingBar(percent int) {
	process := percent * tamanhoBar / 100

	fmt.Printf("\r[")
	for x := 0; x < process; x++ {
		fmt.Printf("#")
	}

	fmt.Printf("] %d%%", percent)

	time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)
}

func main() {
	for x := 0; x < 100; x++ {
		loadingBar(x)
	}

	fmt.Printf("\n")
}

package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

// Func exibeNome - returna um struct cobra
/*
Struct:
   Use: Exemplo de utilização do comando
   Short: Pequena descrição do que o comando faz
   Args: Número de argumentos esperado
   Run: Função com o código do comando
*/
func exibeNome() *cobra.Command {
	var name string

	cmd := &cobra.Command{
		Use:   "hello [nome]",
		Short: "retorna Olá + [nome]",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Olá, %s\n", name)
		},
	}

	cmd.Flags().StringVarP(&name, "name", "n", "Mundo", "flag para concatenar como olá")

	return cmd

}

func main() {

	rootCmd := &cobra.Command{}
	rootCmd.AddCommand(exibeNome())
	rootCmd.Execute()
}

package main

import "fmt"

func main() {
	var orcamento float64

	fmt.Println("Digite seu salário: ")
	fmt.Scanln(&orcamento)

	p55 := orcamento * 0.55
	p30 := orcamento * 0.3
	p10 := orcamento * 0.1
	p005 := orcamento * 0.05

	fmt.Println("#------------------------------#")
	fmt.Println(" Estrátegia de orçamento mensal")
	fmt.Println("#------------------------------#")
	fmt.Println()
	fmt.Printf("55%% do orçamento para gastos fixo: R$: %0.2f\n", p55)
	fmt.Printf("30%% do orçamento para reserva de emergência: R$: %0.2f\n", p30)
	fmt.Printf("10%% do orçamento para lazer: R$: %0.2f\n", p10)
	fmt.Printf("5%% do orçamento para objetivos: R$: %0.2f\n", p005)

}

package main

import "fmt"

// Func retorno nomeado
func converTempo(tempo int) (horas, minutos, segundos int) {
	horas = tempo / 3600
	minutos = (tempo - (horas * 3600)) / 60 // subtraindo as horas do valor maiores
	segundos = tempo % 60
	return horas, minutos, segundos
}

func main() {
	var tempo int
	fmt.Scanln(&tempo)
	horas, minutos, segundos := converTempo(tempo)
	fmt.Printf("%v:%v:%v\n", horas, minutos, segundos)
}

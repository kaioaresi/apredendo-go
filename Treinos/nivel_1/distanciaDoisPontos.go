package main

import (
	"fmt"
	"math"
)

func calculaDistanciaPontos(x1, x2, y1, y2 float64) float64 {
	return math.Sqrt(math.Pow(((x2)-(x1)), 2) + math.Pow(((y2)-(y1)), 2))
}

func main() {
	var x1, y1, x2, y2, distancia float64

	fmt.Scanf("%f %f", &x1, &y1)
	fmt.Scanf("%f %f", &x2, &y2)

	distancia = calculaDistanciaPontos(x1, x2, y1, y2)

	fmt.Printf("%0.4f\n", distancia)
}

package main

import (
	"fmt"
	"math"
)

// Formula maiorAB - MaiorAB=(a+b+abs*(a-b))/2
func maiorAB(a, b int) int {
	return ((a + b + int(math.Abs(float64(a-b)))) / 2)
}

// Formula maiorABC - MaiorABC = (MaiorAB + c + abs(MaiorAB-c))/2;
func maiorabc(a, b, c int) int {
	maiorab := maiorAB(a, b)
	return (maiorab + c + int(math.Abs(float64(maiorab)-float64(c)))) / 2
}

func main() {
	var a, b, c int
	fmt.Scanf("%d %d %d", &a, &b, &c)
	fmt.Printf("%v eh o maior\n", maiorabc(a, b, c))
}

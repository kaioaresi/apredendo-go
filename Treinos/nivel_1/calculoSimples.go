package main

import "fmt"

func calculaProtudo(qtdProduto int, valorProduto float64) float64 {
	return float64(qtdProduto) * valorProduto
}

func main() {
	var codProtudo, qtdProduto int
	var valorProduto, valorTotal float64
	valorTotal = 0

	for i := 0; i < 2; i++ {
		fmt.Scanf("%d %d %f", &codProtudo, &qtdProduto, &valorProduto)
		valorTotal += calculaProtudo(qtdProduto, valorProduto)
	}
	fmt.Printf("VALOR A PAGAR: R$ %0.2f\n", valorTotal)
}

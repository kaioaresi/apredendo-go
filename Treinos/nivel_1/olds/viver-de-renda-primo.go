/*
Kaio quer viver de renda, então vendo algumas videos no youtube ele viu uma formula que mostra o quanto ele precisa investir
para receber uma renda passiva com dividendos de fundos imoviliarios

RDM = renda mesal desejada
preco = preço do fundo
dividendo = dividendo pago pelo fundo imobiliario

formula =>

	(rdm / dividendo) // quantidade de cotas q preciso comprar para viver de renda
	* preco_fiis // valor do fundo imobiliario
*/

package main

import "fmt"

func viverDeRenda(rdm float64, div float64, preco_fiis float64) {
	qtdCotas := rdm / div
	valor := qtdCotas * preco_fiis
	fmt.Printf("Você precisa ter %0.3f cotas, com valor investido de R$: %0.3f para recerber R$: %0.3f\n", qtdCotas, valor, rdm)
}

func main() {
	rdm := 1.000
	div := 0.53
	preco := 90.45
	viverDeRenda(rdm, div, preco)
}

package main

// treino 1 - exibe mesagem
// func main() {
// 	msg := "Olá, mundo!" // Declarando váriavel e atribuindo valor
// 	fmt.Println(msg)     // Exibir uma saída
// }

// ==========================
// Treino 2 - Criar função e exibe msg
// func msg recebe texto tipo string e exibe mensagem
// func msg(texto string) {
// 	fmt.Println(texto)
// }

// func main() {
// 	mensagem := "Hello, world!"
// 	msg(mensagem)
// }

// ==========================

// Treino 3 - Cria func que recebe um array de 3 posições e para cada nome da lita exibe um olá na frente do nome
/* Exemplo:
	["Kaio","Cesar","Santos"]

output:

Olá, Kaio seja bem vindo!
Olá, Cesar seja bem vindo!
Olá, Santos seja bem vindo!
*/

// func saudacao(listaNomes [3]string) {
// 	for _, name := range listaNomes {
// 		fmt.Printf("Olá, %v seja bem-vindo!\n", name)
// 	}
// }

// func main() {
// 	arrayNomes := [3]string{"Kaio", "Cesar", "Dragon"}
// 	saudacao(arrayNomes)
// }

// Resumo: Na func 'saudacao', ela recebe apenas um array de 3 posições, confirme enunciado

// ==========================
// Seguindo a mesma lógica do treino 3, a func deve receber qualquer quantidade de nomes.

// func saudacao(listaNomes []string) {
// 	for _, name := range listaNomes {
// 		fmt.Printf("Olá, %v seja bem-vindo!\n", name)
// 	}
// }

// func main() {
// 	sliceNomes1 := []string{"Kaio", "Cesar", "Dragon"}
// 	sliceNomes2 := []string{"Jose", "Mary"}
// 	saudacao(sliceNomes1)
// 	saudacao(sliceNomes2)
// }

// Resumo: fiz um update na func saudacao para recebe agora slice de nomes, que não possui uma limitação de posições.

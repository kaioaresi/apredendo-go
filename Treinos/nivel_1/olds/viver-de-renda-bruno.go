/*
Formula juros simples =>
juros (R$) = capital (R$) x Taxa (%) x Tempo
*/

package main

import "fmt"

func viverDeRenda(capital float64, taxa float64) {
	juros := capital / (taxa / 100)
	fmt.Printf("Capital total investido deve ser de R$: %0.3f no ano, para ter um renda de R$: %0.3f\n", juros, capital)
}

func main() {
	capitalInvestido := 60.000
	jurosAa := 10.41

	viverDeRenda(capitalInvestido, jurosAa)
	// // t := jurosAa / 100
	// // investimento := capitalInvestido / float64(t)
	// fmt.Printf("%0.3f", investimento)
}

// TODO: aprender a trabalhar com currency

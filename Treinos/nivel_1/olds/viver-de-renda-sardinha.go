/*
formula =>
	valorPagoCota = redimento por cota: R$: 0.42
	rendaDesejada = renda desejada: R$: 4.000

	valorPagoCota / rendaDesejada = 3523,80
*/

package main

import (
	"fmt"
	"math"
)

func viverDeRenda(valorPagoCota float64, rendaDesejada float64, valorPorCota float64) {
	qtdCotas := math.Floor(rendaDesejada / valorPagoCota)
	valorAInvestir := qtdCotas * valorPorCota

	// fmt.Printf("Você precisar comprar %0.3f cotas, para uma renda de R$: %v,\no valor total do investimento é de R$: %0.3f\n", qtdCotas, rendaDesejada, valorAInvestir)
	fmt.Printf("Renda desejado: R$: %v\nQuantidade de cotas: %v\nValor do investimento R$: %0.3f\n", rendaDesejada, qtdCotas, valorAInvestir)
}

func main() {
	valorPagoCota := 0.54
	rendaDesejada := 4000
	valorPorCota := 83.38
	viverDeRenda(valorPagoCota, float64(rendaDesejada), valorPorCota)
}

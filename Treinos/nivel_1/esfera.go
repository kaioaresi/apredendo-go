package main

import (
	"fmt"
	"math"
)

const pi float64 = 3.14159

// formula (4/3) * pi * R ^ 3
func calculaRaio(r float64) float64 {
	return ((4.0 / 3.0) * pi) * (math.Pow(r, 3))
}

func main() {
	var raio float64
	fmt.Scanln(&raio)
	volume := calculaRaio(raio)
	fmt.Printf("VOLUME = %0.3f\n", volume)
}

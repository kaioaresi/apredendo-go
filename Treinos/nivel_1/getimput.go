package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	//######### Nível 1 - Mode simple
	// var nome string
	// fmt.Println("Digite seu nome: ")
	// fmt.Scanln(&nome)
	// fmt.Printf("Seu nome é %v\n", nome)
	// ########################

	//########## Nivel 2
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Digite seu nome: ")
	nome, err := reader.ReadString('\n')
	if err != nil {
		log.Printf("Erro em receber input - %v", err)
	}
	fmt.Printf("Seu nome é : %v", nome)
}

package main

import "fmt"

func checkPar(numero int) bool {
	if (numero % 2) == 0 {
		return true
	}
	return false
}

func main() {
	var a, b, c, d int
	fmt.Scanf("%d %d %d %d", &a, &b, &c, &d)

	if (b > c) && (d > a) && (c+d) > (a+b) && c > 0 && d > 0 && checkPar(a) == true {
		fmt.Println("Valores aceitos")
	} else {
		fmt.Println("Valores nao aceitos")
	}
}

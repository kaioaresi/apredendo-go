package main

import "fmt"

func separaCedulas(valor int) string {
	por100 := int(valor / 100)
	por50 := int((valor % 100) / 50)
	por20 := int((valor%100)%50) / 20
	por10 := int(((valor%100)%50)%20) / 10
	por5 := int((((valor%100)%50)%20)%10) / 5
	por2 := int(((((valor%100)%50)%20)%10)%5) / 2
	por1 := int((((((valor%100)%50)%20)%10)%5)%2) / 1

	msg := fmt.Sprintln(valor)
	msg += fmt.Sprintf("%v nota(s) de R$ 100,00\n", por100)
	msg += fmt.Sprintf("%v nota(s) de R$ 50,00\n", por50)
	msg += fmt.Sprintf("%v nota(s) de R$ 20,00\n", por20)
	msg += fmt.Sprintf("%v nota(s) de R$ 10,00\n", por10)
	msg += fmt.Sprintf("%v nota(s) de R$ 5,00\n", por5)
	msg += fmt.Sprintf("%v nota(s) de R$ 2,00\n", por2)
	msg += fmt.Sprintf("%v nota(s) de R$ 1,00\n", por1)

	return msg
}

func main() {
	var cedulas int

	fmt.Scanln(&cedulas)
	if cedulas > 0 {
		// Separando cedulas
		fmt.Print(separaCedulas(cedulas))
	}

}

package main

import "fmt"

// Calcula triangulo retangulo - a = a * b / 2
func areaTriangulo(base, altura float64) float64 {
	return (base * altura) / 2
}

// Area circulo - A = π . r2
func areaCirculo(raio float64) float64 {
	pi := 3.14159
	return pi * (raio * raio)
}

/* Área do Trapézio - a = b + b / 2 * h

Onde:
	A: área da figura
	B: base maior
	b: base menor
	h: altura
*/
func areaTrapezio(base1, base2, altura float64) float64 {
	return ((base1 + base2) / 2) * altura
}

// Area quadrado - lado^2
func areaQuadrado(lado float64) float64 {
	return lado * lado
}

// Area Retangulo -
func areaRetangulo(base, altura float64) float64 {
	return base * altura
}

func main() {
	var a, b, c float64

	fmt.Scanf("%f %f %f", &a, &b, &c)

	triangulo := areaTriangulo(a, c)
	circulo := areaCirculo(c)
	trapezio := areaTrapezio(a, b, c)
	quadrado := areaQuadrado(b)
	retangulo := areaRetangulo(a, b)

	fmt.Printf("TRIANGULO: %0.3f\nCIRCULO: %0.3f\nTRAPEZIO: %0.3f\nQUADRADO: %0.3f\nRETANGULO: %0.3f\n", triangulo, circulo, trapezio, quadrado, retangulo)
}

// TODO: Refazer usando interfaces

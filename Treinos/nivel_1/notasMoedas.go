package main

import (
	"fmt"
	"math"
)

// Separa notas
func separaCedulas(valor int) []int {
	// Resto da divisão
	mod100 := math.Mod(float64(valor), 100)
	mod50 := math.Mod(mod100, 50)
	mod20 := math.Mod(mod50, 20)
	mod10 := math.Mod(mod20, 10)
	mod5 := math.Mod(mod10, 5)

	// Quantidade de notas por valor
	por100 := valor / 100
	por50 := int(mod100 / 50)
	por20 := int(mod50 / 20)
	por10 := int(mod20 / 10)
	por5 := int(mod10 / 5)
	por2 := int(mod5 / 2)

	return []int{por100, por50, por20, por10, por5, por2}
}

func separadaMoedas(valor float64) []int {
	// Resto da divisão
	mod100 := math.Mod(float64(valor), 100)
	mod50 := math.Mod(mod100, 50)
	mod20 := math.Mod(mod50, 20)
	mod10 := math.Mod(mod20, 10)
	mod5 := math.Mod(mod10, 5)
	mod2 := math.Mod(mod5, 2)

	mod1 := math.Mod(mod2, 1)
	mod050 := math.Mod(mod1, 0.5)
	mod025 := math.Mod(mod050, 0.25)
	mod010 := math.Mod(mod025, 0.10)
	mod005 := math.Mod(mod010, 0.05)

	// Moedas
	por1 := int(mod2 / 1.00)
	por050 := int(float64(mod1) / 0.50)
	por025 := int(float64(mod050) / 0.25)
	por010 := int(float64(mod025) / 0.10)
	por005 := int(float64(mod010) / 0.05)
	por001 := int(float64(mod005 / 0.01))

	// Return
	return []int{por1, por050, por025, por010, por005, por001}
}

// Formata notas
func formatCedulas(sliceCedulasSeparadas []int) string {
	msg := fmt.Sprintln("NOTAS:")
	sliceCedulas := []float64{100, 50, 20, 10, 5, 2}
	i := 0

	for _, value := range sliceCedulasSeparadas {
		msg += fmt.Sprintf("%v nota(s) de R$ %0.2f\n", value, sliceCedulas[i])
		i++
	}

	return msg
}

// Formata moedas
func formatMoedas(sliceMoedas []int) string {
	msg := fmt.Sprintln("MOEDAS:")
	sliceValoresMoedas := []float64{1, 0.50, 0.25, 0.10, 0.05, 0.01}
	i := 0

	for _, value := range sliceMoedas {
		msg += fmt.Sprintf("%v moeda(s) de R$ %0.2f\n", value, sliceValoresMoedas[i])
		i++
	}

	return msg
}

func main() {
	var cedulas float64

	fmt.Scanln(&cedulas)

	// notas
	cedulasSeparas := separaCedulas(int(cedulas))
	cedulasFormatadas := formatCedulas(cedulasSeparas)
	fmt.Print(cedulasFormatadas)

	// Moedas
	separaMoedas := separadaMoedas(cedulas)
	moedasFormatadas := formatMoedas(separaMoedas)
	fmt.Print(moedasFormatadas)
}

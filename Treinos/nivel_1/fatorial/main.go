package main

func fatorialS(numero int) int {
	var result int = 1
	for x := 1; x <= numero; x++ {
		result *= x
	}

	return result
}

func fatorialT(numero int) int {
	if numero == 0 {
		return 1
	}

	return numero * fatorialT(numero-1)
}

// func main() {
// 	fmt.Println("Fatorial simples: ", fatorialS(5))
// 	fmt.Println("Fatorial recurcivo: ", fatorialT(5))
// }

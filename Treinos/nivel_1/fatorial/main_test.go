package main

import (
	"testing"
)

func Test_fatorialS(t *testing.T) {

	tests := []struct {
		name string
		n    int
		want int
	}{
		{name: "", n: 3, want: 6},
		{name: "", n: 5, want: 120},
		{name: "", n: 12, want: 479001600},
		{name: "", n: 0, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fatorialS(tt.n); got != tt.want {
				t.Errorf("fatorialS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_fatorialT(t *testing.T) {
	tests := []struct {
		name   string
		numero int
		want   int
	}{
		{name: "", numero: 3, want: 6},
		{name: "", numero: 5, want: 120},
		{name: "", numero: 12, want: 479001600},
		{name: "", numero: 0, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fatorialT(tt.numero); got != tt.want {
				t.Errorf("fatorialT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkFatorialS(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fatorialS(200)
	}
}
func BenchmarkFatorialT(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fatorialT(200)
	}
}

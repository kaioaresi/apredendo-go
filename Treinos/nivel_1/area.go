package main

import "fmt"

func main() {
	var raio float64
	n := 3.14159

	fmt.Scanln(&raio)

	area := n * (raio * raio)

	fmt.Printf("A=%0.4f\n", area)
}

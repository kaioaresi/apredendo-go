package main

import "fmt"

type elemento struct {
	elementoBase string
	scale        int
}

type personagem struct {
	nome     string
	idade    int8
	elemento elemento
}

func main() {
	skill := elemento{"Fogo", 5}
	player1 := personagem{"Brand", 99, skill}
	fmt.Println(player1)
}

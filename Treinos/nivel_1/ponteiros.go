package main

import "fmt"

var idade int = 50

// recebe um ponteiro e retorna um int
func dobro(valor *int) int {
	*valor = *valor * 2
	return *valor
}

func triplo(valor int) int {
	valor = valor * 3
	return valor
}

func main() {
	fmt.Printf("Idade antes da alteração => %v\n", idade)
	fmt.Printf("Triplicando o valor de idade => %v\n", triplo(idade))
	fmt.Printf("Idade depois do triplo => %v\n", idade)
	fmt.Println()
	fmt.Printf("Idade antes da alteração => %v\n", idade)
	fmt.Printf("Dobrando valor de idade => %v\n", dobro(&idade)) // como a func dobro recebe um ponteiro, para que o valor será alterado é preciso passar o endereço de memoria do valor
	fmt.Printf("Idade depois do dobro alterações => %v\n", idade)
}

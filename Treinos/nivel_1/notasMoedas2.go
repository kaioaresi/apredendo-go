package main

import (
	"fmt"
	"math"
)

// Decompoe valor
func decomposicao(valor float64) []int {
	// Resto da divisão de valor
	resto100 := math.Mod(valor, 100)
	resto50 := math.Mod(resto100, 50)
	resto20 := math.Mod(resto50, 20)
	resto10 := math.Mod(resto20, 10)
	resto5 := math.Mod(resto10, 5)
	resto2 := math.Mod(resto5, 2)
	resto1 := math.Mod(resto2, 1)
	resto050 := math.Mod(resto1, 0.50)
	resto025 := math.Mod(resto050, 0.25)
	resto010 := math.Mod(resto025, 0.10)
	resto005 := math.Mod(resto025, 0.05)

	// Divisão valor
	por100 := valor / 100
	por50 := resto100 / 50
	por20 := resto50 / 20
	por10 := resto20 / 10
	por5 := resto10 / 5
	por2 := resto5 / 2
	por1 := resto2 / 1
	por050 := resto1 / 0.50
	por025 := resto050 / 0.25
	por010 := resto025 / 0.10
	por005 := resto010 / 0.05
	// por001 := math.Round(resto005 / 0.01) // arendorando para cima não suportado em go 1.8.1
	por001 := ((resto005 / 0.01) + 0.5) // aredendondo para cima

	return []int{int(por100), int(por50), int(por20), int(por10), int(por5), int(por2), int(por1), int(por050), int(por025), int(por010), int(por005), int(por001)}
}

// Formata saída de notas e moedas
func formataNotasMoedas(sliceNotasMoedas []int) string {
	sliceValores := []float64{100, 50, 20, 10, 5, 2, 1, 0.50, 0.25, 0.10, 0.05, 0.01}
	i := 0
	msg := fmt.Sprint("NOTAS:\n")

	for _, qtdNotas := range sliceNotasMoedas[:6] {
		msg += fmt.Sprintf("%v nota(s) de R$ %0.2f\n", qtdNotas, sliceValores[i])
		i++
	}

	msg += fmt.Sprint("MOEDAS:\n")

	for _, qtdMoedas := range sliceNotasMoedas[6:] {
		msg += fmt.Sprintf("%v moeda(s) de R$ %0.2f\n", qtdMoedas, sliceValores[i])
		i++
	}

	return msg
}

func main() {
	var valor float64

	fmt.Scanln(&valor)

	fmt.Print(formataNotasMoedas(decomposicao(valor)))
}

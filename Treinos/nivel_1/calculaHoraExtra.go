/* Objetivo: Criar um software que calcule o valor da minha hora extra e trabalhada
 */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func getSalario() float64 {
	salarioBruto := bufio.NewScanner(os.Stdin)
	fmt.Println("Digite salário bruto: ")
	salarioBruto.Scan()
	salario, err := strconv.ParseFloat(salarioBruto.Text(), 64)
	if err != nil {
		log.Printf("Você deve digitar um número!")
	}
	return salario
}

func getHorasExtraTrabalhadas() float64 {
	qtdHorasExtras := bufio.NewScanner(os.Stdin)
	fmt.Println("Quantas horas extra foram feitas?: ")
	qtdHorasExtras.Scan()
	horasExtras, err := strconv.ParseFloat(qtdHorasExtras.Text(), 64)
	if err != nil {
		log.Printf("Você deve digitar um número!")
	}
	return horasExtras
}

func calculaHoraBase(salario float64) float64 {
	horaBase := salario / 220
	return horaBase
}

func calculaHoraExtra50(horaBase float64) float64 {
	horaExtra := (horaBase * 0.5) + horaBase
	return horaExtra
}

func calculaHoraExtra100(horaBase float64) float64 {
	horaExtra := horaBase * 1
	return horaExtra
}

func valorReceber(valorHoraExtra float64, qtdHorasTrabalhadas float64, salarioBruto float64) float64 {
	valorReceber := (valorHoraExtra * qtdHorasTrabalhadas) + salarioBruto
	return valorReceber
}

func main() {
	salario := getSalario()
	qtdHorasExtras := getHorasExtraTrabalhadas()
	valorHora := calculaHoraBase(salario)
	valorHoraExtra50 := calculaHoraExtra50(valorHora)
	valorHoraExtra100 := calculaHoraExtra100(valorHora)
	valorFinal := valorReceber(valorHoraExtra50, qtdHorasExtras, salario)

	fmt.Printf("Salário R$: %.2f e %v horas extras\n", salario, qtdHorasExtras)
	fmt.Printf("Valor hora base: R$: %.2f\n", valorHora)
	fmt.Printf("Valor hora extra 50: %.2f\n", valorHoraExtra50)
	fmt.Printf("Valor hora extra 100: %.2f\n", valorHoraExtra100)
	fmt.Printf("Valor a receber é de %.2f\n", valorFinal)
}

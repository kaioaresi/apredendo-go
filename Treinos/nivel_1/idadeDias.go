package main

import "fmt"

func converteAnoMesDia(idade int) (ano, mes, dia int) {
	ano = idade / 365
	mes = (idade % 365) / 30
	dia = ((idade % 365) % 30)
	return ano, mes, dia
}

func formatConversao(ano, mes, dia int) string {
	msg := fmt.Sprintf("%v ano(s)\n", ano)
	msg += fmt.Sprintf("%v mes(es)\n", mes)
	msg += fmt.Sprintf("%v dia(s)\n", dia)
	return msg
}

func main() {
	var idade, ano, mes, dia int
	fmt.Scanln(&idade)
	ano, mes, dia = converteAnoMesDia(idade)

	fmt.Print(formatConversao(ano, mes, dia))
}

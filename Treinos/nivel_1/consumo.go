package main

import "fmt"

func main() {
	var distanciaKM int
	var combustivelGasto float64
	fmt.Scanln(&distanciaKM)
	fmt.Scanln(&combustivelGasto)
	consumo := float64(distanciaKM) / combustivelGasto
	fmt.Printf("%0.3f km/l\n", consumo)
}

package main

import "fmt"

func main() {
	var nFuncionario, horasTra int
	var valorHora, salario float64

	fmt.Scanln(&nFuncionario)
	fmt.Scanln(&horasTra)
	fmt.Scanln(&valorHora)

	salario = float64(horasTra) * valorHora

	fmt.Printf("NUMBER = %v\nSALARY = U$ %0.2f\n", nFuncionario, salario)
}

package main

import (
	"fmt"
	"math"
)

// Delta restorna um float64
func formulaDelta(a, b, c float64) float64 {
	// Formula de delta b² - 4*a*c
	return math.Pow(b, 2) - (4 * a * c)
}

// Bhaskara
func formulaBhaskara(a, b, c, delta float64) (x1 float64, x2 float64) {
	// Formula de Bhaskara = (-b +- raiz(delta)) / (2.a)

	x1 = ((-b) + (math.Sqrt(delta))) / (2 * a)
	x2 = ((-b) - (math.Sqrt(delta))) / (2 * a)

	return x1, x2
}

func main() {
	var a, b, c float64

	fmt.Scanf("%f %f %f", &a, &b, &c)

	delta := formulaDelta(a, b, c)
	x1, x2 := formulaBhaskara(a, b, c, delta)

	if math.IsNaN(x1) == true || math.IsNaN(x2) {
		fmt.Println("Impossivel calcular")
	} else {
		fmt.Printf("R1 = %0.5f\nR2 = %0.5f\n", x1, x2)
	}
}

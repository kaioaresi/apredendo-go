package main

import "fmt"

func main() {
	var nome string
	var salarioFixo, totalVendas, salarioBunos float64

	fmt.Scanln(&nome)
	fmt.Scanln(&salarioFixo)
	fmt.Scanln(&totalVendas)

	salarioBunos = (totalVendas * 0.15) + salarioFixo

	fmt.Printf("TOTAL = R$ %0.2f\n", salarioBunos)
}

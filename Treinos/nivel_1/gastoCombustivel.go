package main

import "fmt"

func gastoCombustivel(tempo, velocidade int) float64 {
	return float64((tempo * velocidade)) / float64(12)
}

func main() {
	var tempoViagem, velocidade int
	fmt.Scanln(&tempoViagem)
	fmt.Scanln(&velocidade)
	fmt.Printf("%0.3f\n", gastoCombustivel(tempoViagem, velocidade))
}

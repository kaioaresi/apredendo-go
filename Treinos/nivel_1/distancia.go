package main

import "fmt"

func main() {
	var distancia float64

	fmt.Scanln(&distancia)

	fmt.Printf("%v minutos\n", distancia*2)
}

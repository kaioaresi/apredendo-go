package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func Hello() *cobra.Command {
	// Basico 1
	// return &cobra.Command{
	// 	Use:   "hello [name]",
	// 	Short: "Retorna mensagem 'Olá, [name]'",
	// 	Args:  cobra.ExactArgs(1),
	// 	Run: func(cmd *cobra.Command, args []string) {
	// 		fmt.Printf("Olá, %v\n", args[0])
	// 	},
	// }

	// Basico 2
	var name string

	cmd := &cobra.Command{
		Use:   "hello [name]",
		Short: "Retorna mensagem 'Olá, [name]'",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Olá, %v\n", name)
		},
	}

	// StringVarP - nome da flag, nome curto da flag, valor default, pequena descrição sobre o que a flag faz
	cmd.Flags().StringVarP(&name, "name", "n", "Mundo", "flag para concatener com Olá")
	return cmd
}

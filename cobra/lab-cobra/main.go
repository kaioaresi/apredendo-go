package main

import (
	"lab-cobra/cmd"

	"github.com/spf13/cobra"
)

func main() {
	rootCmd := &cobra.Command{}

	rootCmd.AddCommand(cmd.Hello())

	rootCmd.Execute()
}

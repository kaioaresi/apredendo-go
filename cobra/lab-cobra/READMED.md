# Cobra tutorial

## Preparando ambiente
Preparando estrutura de arquivos

```
├── READMED.md
├── cmd
│   └── Hello.go
└── main.go
```

Iniciando module

```
go mod init lab-cobra
```

Add lib externa cobra e baixando

```
go get -u github.com/spf13/cobra/cobra
go mod tidy
```

__Func Hello()__

Modelo básico 1

```golang
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func Hello() *cobra.Command {
	
	return &cobra.Command{
		Use:   "hello [name]",
		Short: "Retorna mensagem 'Olá, [name]'",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Olá, %v\n", args[0])
		},
	}
}
```

Modelo básico 2

```
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func Hello() *cobra.Command {
	var name string

	cmd := &cobra.Command{
		Use:   "hello [name]",
		Short: "Retorna mensagem 'Olá, [name]'",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Olá, %v\n", name)
		},
	}

	// StringVarP - nome da flag, nome curto da flag, valor default, pequena descrição sobre o que a flag faz
	cmd.Flags().StringVarP(&name, "name", "n", "Mundo", "flag para concatener com Olá")
	
	return cmd
}
```



__Func main__

```golang
package main

import (
	"lab-cobra/cmd"

	"github.com/spf13/cobra"
)

func main() {
	rootCmd := &cobra.Command{}

	rootCmd.AddCommand(cmd.Hello())

	rootCmd.Execute()
}
```

Como de execução 

```
go run main.go hello <NOME>|--help
```


---

# Referencias

- [Doc. Cobra](https://github.com/spf13/cobra/blob/main/user_guide.md)
- [Criando command line interface cli com cobra](https://aprendagolang.com.br/2021/11/17/criando-command-line-interface-cli-com-cobra/)

# Golang

## What is Go?

- Go is a "fast, statically typed, compiled language"
- Go is a general purpose language
- Go has built-in testing support
- Go is as objetc-oriented lagnguage (in it's own way)

## Ways to declare variables

Exemple:

```golang
package main

import "fmt"

func main() {
	var nome string = "Kaio"
	var sobreNome = "Santos"
	var idade int
	idade = 900
	nacionalidade := "BR"
	fmt.Println(nome, sobreNome, idade, nacionalidade)
}
```

## Printing & Formatting Strings

```golang
	name := "Kaio"
	lastName := "Santos"
	age := 900

	// Outputs padrões
	fmt.Print("No, create")
	fmt.Print(" new line")
	fmt.Println("Create")
	fmt.Println("new line")

	// Formatando outputs
	fmt.Println("First name: ", name, "Last name:", lastName)
	fmt.Printf("Fist name: %v, Last name: %v \n", name, lastName)
	fmt.Printf("Fist name: %q, Last name: %v, Age: %q \n", name, lastName, age) // "" no valor, funciona apenas com string
	fmt.Printf("Your Score is %f", 990.9)
	fmt.Printf("Your Score is %0.2f \n", 990.9) // duas casas decimais

	// Salvando outputs formatados
	texto_formatado := fmt.Sprintf("Fist name: %q, Last name: %v, Age: %T \n", name, lastName, age)
	fmt.Println("Output Sprintf: ", texto_formatado)
```

## Arrays, Slices & Range

### Arrays

```golang
func main() {
	// Arrays
	// var arrayTest [3]int = [3]int{1, 2, 3} // jeito mais completo de se declarar um array
	arrayInt := [3]int{1, 2, 3} // jeito curto de se declarar um array
	arrayString := [4]string{"Kaio", "Cesar", "Santos", "Dragon"}

	fmt.Println(arrayInt, len(arrayInt))       // exibindo o array e seu tamanho
	fmt.Println(arrayString, len(arrayString)) // exibindo o array e seu tamanho
	arrayInt[1] = 10                           // update valor possição 1 do array
	fmt.Println(arrayInt, len(arrayInt))       // exibindo o array e seu tamanho
}
```

### Slices

```golang
func main() {
	// Slices
	slices1 := []int{4, 5, 6} // Para declarar um slice, não deve-se informar o tamanho do slice.
	fmt.Println(slices1)
	slices1 = append(slices1, 100) // appende um novo valor ao slice
	fmt.Println(slices1)
	slices1[2] = 500
	fmt.Println(slices1) // update valor na posição dois do slice
}
```

### Range

```golang
func main() {
	// Range
	range1 := arrayString[1:3]        // aguardando os valor do arrayString da posição 1-3
	range2 := arrayString[1:]         // aguarda valor partindo da posição 1 até o final do array
	range3 := arrayString[:2]         // aguardando valor invertido da posição 2 a até o inicio do array
	fmt.Println("Range 1-3:", range1) // Mesmo declarando que vamor ler o array da posição 1 até a posição 3, o valor da posição 3 não é exibido
	fmt.Println("Range 1-:", range2)
	fmt.Println("Range -2:", range3)
}
```

## [The Standard Library](https://pkg.go.dev/std)

### Strings

```golang
import (
	"fmt"
	"strings"
)

func main() {
	mensageTeste := "Hello, world!"
	fmt.Println(strings.Contains(mensageTeste, "Hello"))          // Retorna um bool se existe ou não explicitamente a palavra na var
	fmt.Println(strings.ReplaceAll(mensageTeste, "Hello", "Eai")) // Exibe todas palvra do match para "Eai"
	fmt.Println("Upper = ", strings.ToUpper(mensageTeste))
	fmt.Println("Lower = ", strings.ToLower(mensageTeste))
	fmt.Println("Title = ", strings.ToTitle(mensageTeste))
	fmt.Println("Split", strings.Split(mensageTeste, " "))
}
```

### Sort

```golang
import (
	"fmt"
	"sort"
)

func main() {

	// Sort
	age := []int{23, 412, 123, 12, 46, 67}
	sort.Ints(age)
	fmt.Println(age)

	index := sort.SearchInts(age, 23)
	fmt.Println(index)
}
```

## Loops


### For


Exemple 1:

```golang
func main() {
	// basic for
	x := 0
	for x < 5 {
		fmt.Println("Valor de x é", x)
		x++
	}
}
```

Exemple 2:

```golang

func main() {
	// For sort
	for i := 0; i < 5; i++ {
		fmt.Println("Valor de x é", i)
	}
}
```

```golang
func main() {
	// Varendo slice
	sliceNames := []string{"Jonh", "Cesar", "Dragon", "Mary"}

	for i := 0; i < len(sliceNames); i++ {
		fmt.Printf("The name is %q \n", sliceNames[i])
	}
}
```

Exemple 3:

```golang
func main() {
	// Varendo slice
	sliceNames := []string{"Jonh", "Cesar", "Dragon", "Mary"}

	for index, names := range sliceNames {
		fmt.Printf("Index is %v and names is %v \n", index, names)
	}
}
```

Exemple 4:

```golang
func main() {
	// Varendo slice
	sliceNames := []string{"Jonh", "Cesar", "Dragon", "Mary"}

	for _, names := range sliceNames {
		fmt.Printf("Names is %v \n", names)
	}
}
```

## Booleans & Conditionals

### if, else if, else

```golang
func main() {
	age := 45
	if age > 50 {
		fmt.Println("Age is more then 50")
	} else if age < 50 {
		fmt.Println("Age is less then 50")
	} else {
		fmt.Println("Age is not less 50")
	}
}
```

### for if

```golang
func main() {

	sliceName := []string{"Dragon", "Kaio", "Mary"}

	for index, values := range sliceName {
		if index == 1 {
			fmt.Println("O index 1 tem o value", values)
			continue
		}

		fmt.Printf("Index = %v com value %v \n", index, values)
	}
}
```

## Using Functions

Exemple 1:

```golang
func boasVindas(nome string) {
	fmt.Printf("Bem vindo! %v \n", nome)
}

func tchau(nome string) {
	fmt.Printf("Até logo! %v \n", nome)
}

func main() {
	boasVindas("Kaio")
	tchau("Mary")
}
```

Exemple 2:

```golang
func boasVindas(nome string) {
	fmt.Printf("Bem vindo! %v \n", nome)
}

func multipesFunc(sliceName []string, f func(string)) {
	for _, values := range sliceName {
		f(values)
	}
}

func main() {
	names := []string{"Kaio", "Mary", "Dragon", "Rock"}
	multipesFunc(names, boasVindas) // Na func multipesFunc recebemos dois argumentos, um slice e uma func, a função no momento da declaração não precisa passa com '()', basta apenas o nume
}
```

Exemple 3:

```golang
// Recebe um float64 e retorna um float64
func circleArea(raio float64) float64 {
	return math.Pi * raio * raio
}

func main() {
	a1 := circleArea(5.5)
	b1 := circleArea(7.5)
	fmt.Printf("%0.2f %0.2f\n", a1, b1)

}
```

## Multiple Return Values

```golang
// Recebe uma string e retorna duas string
func getInitials(n string) (string, string) {
	sliceN := strings.ToUpper(n)
	splitString := strings.Split(sliceN, " ")

	var initials []string

	for _, v := range splitString {
		initials = append(initials, v[:1])
	}

	if len(initials) > 1 {
		return initials[0], initials[1]
	}

	return initials[0], "_"
}

func main() {
	fn1, sn1 := getInitials("Kaio Dragon")
	fmt.Println(fn1, sn1)
}
```

## Package Scope

Exemplo

Arquivo greetings

```golang
var points = []int{20, 10, 30, 50, 60}

func sayHello(palavra string) {
	fmt.Println("Hello", palavra)
}

func funcScore() {
	fmt.Println("Your score is: ", score) // o inverso também funciona, podemos utilizar uma var em scopo global de outros arquivo, mesmo do main.go
}
```

Arquivo main

```golang
var score = 80.1

func main() {
	sayHello("Kaio")           // Aqui podemos chamar a func que está no arquivo greetings sem problemas
	for _, v := range points { // aqui é outro exempo que também podemos utilizar vars que estão em scopo global em outros arquivos.
		fmt.Println(v)
	}

	funcScore()
}
```

> Para que tudo funcione como deveria, deve-se passar o nome de ambos arquivos ao executar o run exemplo: `go run main.go greetings.go`


## Maps

Exemplo 1:

```golang
func main() {
	// estrutura padrão do map: map[<TYPE KEY>]TYPE VALUE{<KEY>:<VALUE>}
	menu := map[string]float64{
		"Sabão":  5.70,
		"Arroz":  10.91,
		"Frango": 15.55, // Sempre add um ',' no final da "lista" do map
	}

	fmt.Println(menu)          // exibe todo map
	fmt.Println(menu["Arroz"]) // exibe atravez de uma key o valor da key

}
```

Exemplo 2 - loop

```golang
func main() {

	// Ints as key type
	lista := map[int]string{
		1: "Kaio",
		2: "Mary",
		3: "Dragon",
	}

	fmt.Println(lista)
	fmt.Println(lista[1])

	for k, v := range lista {
		fmt.Println(k, "-", v)
	}
}
```


Exemplo 3 - loop

```golang
func main() {

	menu := map[string]float64{
		"Sabão":  5.70,
		"Arroz":  10.91,
		"Frango": 15.55,
	}

	// Loop map
	for chave, value := range menu {
		fmt.Println(chave, "-", value)
	}

}
```

Exemplo 4 - Update map

```golang
func main() {

	// Ints as key type
	lista := map[int]string{
		1: "Kaio",
		2: "Mary",
		3: "Dragon",
	}

	// Update value
	lista[1] = "Cesar"
	fmt.Println(lista)
}
```

## Pass By Value


Go makes "copies" of values when passed into functions

Type of values

- Non-Pointer values
	- String
	- int
	- floats
	- booleans
	- Arrays
	- Structs

Non-Pointer values são tipos cujo a var original não alterada e apenas `copiada`.

Exemplo:

```golang
func upperText(text string) string {
	text = strings.ToUpper(text)
	return text
}

func main() {
	name := "Kaio"
	fmt.Println("Valor ser exibido em uppper: ", upperText(name))
	fmt.Println("Valor original não é alterado: ", name)
}
```
- Non-Pointer values
	- String
	- int
	- floats
	- booleans
	- Arrays
	- Structs

![Non-Pointer values](./img/non-pointer-values.jpeg)


> Resumo: A variavel é clonada para uma nova possição na memoria, não alterando o valor da var original

---

- Pointer wrapper values
	- Slices
	- Maps
	- Functions

Pointer wrapper values são tipos que são referenciados (pointer), alterando seus valores

```golang
func changeValue(text map[string]float64) {
	text["RedBull"] = 99.99
}

func main() {
	// Pointer wrapper values
	mapTest := map[string]float64{
		"RedBull": 5.5,
		"Carne":   500.5,
	}

	fmt.Println("Valor original: ", mapTest)
	changeValue(mapTest) // o valor é refereciado no endereço de memoria, alterando assim o valor da posição
	fmt.Println("Valor alterado pelo func: ", mapTest)
}
```

![Pointer wrapper values](./img/pointer-wrapper-values.jpeg)

> Resumo: Esses tipos de variaveis quando tem seu valor alterado, ele não clona a variavel original e sim a altera, pois existe um apontamento para a mesma conforme image acima.


## Pointers

```golang
func changeName(x *string) {
	// a func vai receber um ponteiro e irá alterar o valor do ponteiro
	*x = "Dragon"
}
func main() {
	name := "Cesar"

	fmt.Println(name)
	fmt.Println("Endereço de memoria da var name: ", &name) // exibindo o endereço de memoria da variavel
	m := &name                                              // guardando o endereço de memoria de name
	fmt.Println("Endereço de m é:", m)
	fmt.Println("O valor do endereço de memoria é:", *m) // exibindo o valor do endereço de memoria de m

	fmt.Printf("Valor de name original: ", name)
	changeName(m) // alterando o valor do ponteiro
	fmt.Printf("Valor alterado: ", name)

}
```

![Var name original](./img/pointer-original.jpeg)

Agora vamos alterar o valor direto pelo ponteiro

![Var name alterada](./img/pointer-valor-alterado.jpeg)


## Structs & Custom Types

```golang
type bill struct {
	name  string
	items map[string]float64
	tip   float64
}

// Recebe um parametros name do tipo string e retorna um type bill que criamos anteriormente
func newBill(name string) bill {
	b := bill{
		name:  name,
		items: map[string]float64{},
		tip:   0,
	}

	return b
}
```

## Receiver Functions

Func bill

```golang
type bill struct {
	name  string
	items map[string]float64
	tip   float64
}

// Recebe um parametros name do tipo string e retorna um type bill que criamos anteriormente
func newBill(name string) bill {
	b := bill{
		name:  name,
		items: map[string]float64{"bolo": 5.10, "tapioca": 4.70, "vitamina": 9.10},
		tip:   0,
	}

	return b
}

// format the bill
func (b bill) format() string {
	fs := "Bill breakdown \n"
	var total float64 = 0

	// list items
	for k, v := range b.items {
		fs += fmt.Sprintf("%-25v...$ %v \n", k+":", v)
		total += v
	}

	// total
	fs += fmt.Sprintf("%-25v...$ %0.2f", "total:", total)

	return fs
}

```

Func main

```golang
func main() {
	clientBill := newBill("Kaio Dragon")
	fmt.Println(clientBill.format())
}
```

```output
Bill breakdown 
bolo:                    ...$ 5.1 
tapioca:                 ...$ 4.7 
vitamina:                ...$ 9.1 
total:                   ...$ 18.90
```

## Switch Statements

```golang
func main() {
	i := 2
	fmt.Println("Qual o melhor tipo de comida? ")
	switch i {
	case 1:
		fmt.Println("Hamburguer!")
	case 2:
		fmt.Println("Pizza!")
	default:
		fmt.Println("Opção invalida!")
	}

}
```

## Parsing Floats

```golang
func main(){
	mensage := "Mensagem teste!"
	mensageByte := []byte(mensage)
	fmt.Println(mensageByte)
}
```

## Saving files

```golang
func saveFile(mensage string) {
	mensageByte := []byte(mensage)

	err := os.WriteFile("save-file/teste.txt", mensageByte, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("File saved!")
}

func main() {
	mensage := "Mensagem de teste"
	saveFile(mensage)
}
```

## Interfaces


---

# References

- [The net ninja](https://www.youtube.com/watch?v=etSN4X_fCnM&list=PL4cUxeGkcC9gC88BEo9czgyS72A3doDeM)
- [golang tutorials](https://github.com/iamshaunjp/golang-tutorials)
- [fmt packages](https://pkg.go.dev/fmt)
- [The Standard Library](https://pkg.go.dev/std)
- [switch case](https://gobyexample.com/switch)

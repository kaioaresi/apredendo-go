package main

import "fmt"

func main() {
	name := "Kaio"
	lastName := "Santos"
	age := 900

	// Outputs padrões
	fmt.Print("No, create")
	fmt.Print(" new line")
	fmt.Println("Create")
	fmt.Println("new line")

	// Formatando outputs
	fmt.Println("First name: ", name, "Last name:", lastName)
	fmt.Printf("Fist name: %v, Last name: %v \n", name, lastName)
	fmt.Printf("Fist name: %q, Last name: %v, Age: %q \n", name, lastName, age) // "" no valor, funciona apenas com string
	fmt.Printf("Your Score is %f", 990.9)
	fmt.Printf("Your Score is %0.2f \n", 990.9) // duas casas decimais

	// Salvando outputs formatados
	texto_formatado := fmt.Sprintf("Fist name: %q, Last name: %v, Age: %T \n", name, lastName, age)
	fmt.Println("Output Sprintf: ", texto_formatado)
}

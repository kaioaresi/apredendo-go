package main

import "fmt"

func main() {
	// age := 45
	// if age > 50 {
	// 	fmt.Println("Age is more then 50")
	// } else if age < 50 {
	// 	fmt.Println("Age is less then 50")
	// } else {
	// 	fmt.Println("Age is not less 50")
	// }

	sliceName := []string{"Dragon", "Kaio", "Mary"}

	for index, values := range sliceName {
		if index == 1 {
			fmt.Println("O index 1 tem o value", values)
			continue
		}

		fmt.Printf("Index = %v com value %v \n", index, values)
	}
}

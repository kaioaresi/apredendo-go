package main

import (
	"fmt"
	"sort"
)

func main() {
	// Strings
	// mensageTeste := "Hello, world!"
	// fmt.Println(strings.Contains(mensageTeste, "Hello"))          // Retorna um bool se existe ou não explicitamente a palavra na var
	// fmt.Println(strings.ReplaceAll(mensageTeste, "Hello", "Eai")) // Exibe todas palvra do match para "Eai"
	// fmt.Println("Upper = ", strings.ToUpper(mensageTeste))
	// fmt.Println("Lower = ", strings.ToLower(mensageTeste))
	// fmt.Println("Title = ", strings.ToTitle(mensageTeste))
	// fmt.Println("Split", strings.Split(mensageTeste, " "))

	// Sort
	age := []int{23, 412, 123, 12, 46, 67}
	sort.Ints(age)
	fmt.Println(age)

	index := sort.SearchInts(age, 23)
	fmt.Println(index)
}

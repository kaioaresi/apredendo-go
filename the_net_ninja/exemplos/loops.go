package main

import "fmt"

func main() {
	// // basic for
	// x := 0
	// for x < 5 {
	// 	fmt.Println("Valor de x é", x)
	// 	x++
	// }

	// // For sort
	// for i := 0; i < 5; i++ {
	// 	fmt.Println("Valor de x é", i)
	// }

	// Varendo slice
	sliceNames := []string{"Jonh", "Cesar", "Dragon", "Mary"}

	// for i := 0; i < len(sliceNames); i++ {
	// 	fmt.Printf("The name is %q \n", sliceNames[i])
	// }

	// for index, names := range sliceNames {
	// 	fmt.Printf("Index is %v and names is %v \n", index, names)
	// }

	for _, names := range sliceNames {
		fmt.Printf("Names is %v \n", names)
	}
}

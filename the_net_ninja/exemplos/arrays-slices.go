package main

import "fmt"

func main() {
	// Arrays
	// var arrayTest [3]int = [3]int{1, 2, 3} // jeito mais completo de se declarar um array
	arrayInt := [3]int{1, 2, 3} // jeito curto de se declarar um array
	arrayString := [4]string{"Kaio", "Cesar", "Santos", "Dragon"}

	fmt.Println("Array int", arrayInt, len(arrayInt))          // exibindo o array e seu tamanho
	fmt.Println("Array string", arrayString, len(arrayString)) // exibindo o array e seu tamanho
	arrayInt[1] = 10                                           // update valor possição 1 do array
	fmt.Println(arrayInt, len(arrayInt))                       // exibindo o array e seu tamanho

	// Slices
	slices1 := []int{4, 5, 6} // Para declarar um slice, não deve-se informar o tamanho do slice.
	fmt.Println("Slice 1: ", slices1)
	slices1 = append(slices1, 100) // appende um novo valor ao slice
	fmt.Println("Slice appended", slices1)
	slices1[2] = 500
	fmt.Println("Slice updated", slices1) // update valor na posição dois do slice

	// Range
	range1 := arrayString[1:3]        // aguardando os valor do arrayString da posição 1-3
	range2 := arrayString[1:]         // aguarda valor partindo da posição 1 até o final do array
	range3 := arrayString[:2]         // aguardando valor invertido da posição 2 a até o inicio do array
	fmt.Println("Range 1-3:", range1) // Mesmo declarando que vamor ler o array da posição 1 até a posição 3, o valor da posição 3 não é exibido
	fmt.Println("Range 1-:", range2)
	fmt.Println("Range -2:", range3)
}

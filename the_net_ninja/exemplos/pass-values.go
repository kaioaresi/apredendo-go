package main

import (
	"fmt"
	"strings"
)

// Non-Pointer values
func upperText(text string) string {
	text = strings.ToUpper(text)
	return text
}

func changeValue(text map[string]float64) {
	text["RedBull"] = 99.99
}

func main() {
	//Non-Pointer values
	// name := "Kaio"
	// fmt.Println("Valor ser exibido em uppper: ", upperText(name))
	// fmt.Println("Valor original não é alterado: ", name)

	// Pointer wrapper values
	mapTest := map[string]float64{
		"RedBull": 5.5,
		"Carne":   500.5,
	}

	fmt.Println("Valor original: ", mapTest)
	changeValue(mapTest)
	fmt.Println("Valor alterado pelo func: ", mapTest)
}

package main

import "fmt"

var score = 80.1

func main() {
	sayHello("Kaio") // Aqui podemos chamar a func que está no arquivo greetings sem problemas

	for _, v := range points { // aqui é outro exempo que também podemos utilizar vars que estão em scopo global em outros arquivos.
		fmt.Println(v)
	}

	funcScore()
}

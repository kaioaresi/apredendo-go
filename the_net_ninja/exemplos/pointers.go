package main

import "fmt"

func changeName(x *string) {
	// a func vai receber um ponteiro e irá alterar o valor do ponteiro
	*x = "Dragon"
}
func main() {
	name := "Cesar"

	fmt.Println(name)
	fmt.Println("Endereço de memoria da var name: ", &name) // exibindo o endereço de memoria da variavel
	m := &name                                              // guardando o endereço de memoria de name
	fmt.Println("Endereço de m é:", m)
	fmt.Println("O valor do endereço de memoria é:", *m) // exibindo o valor do endereço de memoria de m

	fmt.Printf("Valor de name original: ", name)
	changeName(m) // alterando o valor do ponteiro
	fmt.Printf("Valor alterado: ", name)

}

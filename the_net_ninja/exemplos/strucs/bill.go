package main

import (
	"fmt"
	"os"
)

type bill struct {
	name  string
	items map[string]float64
	tip   float64
}

// Recebe um parametros name do tipo string e retorna um type bill que criamos anteriormente
func newBill(name string) bill {
	b := bill{
		name:  name,
		items: map[string]float64{},
		tip:   0,
	}

	return b
}

// format the bill
func (b *bill) format() string {
	fs := "Bill breakdown \n\n"
	var total float64 = 0

	// list items
	for k, v := range b.items {
		fs += fmt.Sprintf("%-25v...$ %v \n", k+":", v)
		total += v
	}

	// Tip
	fs += fmt.Sprintf("%-25v...$ %0.2f\n", "tip:", b.tip)

	// total
	fs += fmt.Sprintf("%-25v...$ %0.2f", "total:", total+b.tip)

	return fs
}

// Update tip - apontando para o ponteiro realizamos o update do valor armazenado em `tip`
func (b *bill) updateTip(tip float64) {
	b.tip = tip
}

// add an item to the bill
func (b *bill) addItem(name string, price float64) {
	b.items[name] = price
}

// save bill
func (b *bill) save() {
	// Para salvar em um arquivo, devemos converter para byte code
	data := []byte(b.format())

	// parametros path, dado, permissões
	err := os.WriteFile("strucs/bills/"+b.name+".txt", data, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("Bill was saved to file")
}

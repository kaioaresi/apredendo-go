package main

import (
	"fmt"
	"math"
)

func boasVindas(nome string) {
	fmt.Printf("Bem vindo! %v \n", nome)
}

func tchau(nome string) {
	fmt.Printf("Até logo! %v \n", nome)
}

func multipesFunc(sliceName []string, f func(string)) {
	for _, values := range sliceName {
		f(values)
	}
}

// Recebe um float64 e retorna um float64
func circleArea(raio float64) float64 {
	return math.Pi * raio * raio
}

func main() {
	names := []string{"Kaio", "Mary", "Dragon", "Rock"}
	multipesFunc(names, boasVindas)

	a1 := circleArea(5.5)
	b1 := circleArea(7.5)

	fmt.Printf("%0.2f %0.2f\n", a1, b1)

}

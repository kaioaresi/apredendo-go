package main

import "fmt"

func main() {

	menu := map[string]float64{
		"Sabão":  5.70,
		"Arroz":  10.91,
		"Frango": 15.55,
	}

	fmt.Println(menu)          // exibe todo map
	fmt.Println(menu["Arroz"]) // exibe atravez de uma key o valor da key

	// Loop map
	for chave, value := range menu {
		fmt.Println(chave, "-", value)
	}

	// Ints as key type
	lista := map[int]string{
		1: "Kaio",
		2: "Mary",
		3: "Dragon",
	}

	fmt.Println(lista)
	fmt.Println(lista[1])

	for k, v := range lista {
		fmt.Println(k, "-", v)
	}

	// Update value
	lista[1] = "Cesar"
	fmt.Println(lista)
}

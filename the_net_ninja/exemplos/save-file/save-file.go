package main

import (
	"fmt"
	"os"
)

func saveFile(mensage string) {
	mensageByte := []byte(mensage)

	err := os.WriteFile("save-file/teste.txt", mensageByte, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("File saved!")
}

func main() {
	mensage := "Mensagem de teste"
	saveFile(mensage)
}

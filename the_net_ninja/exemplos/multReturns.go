package main

import (
	"fmt"
	"strings"
)

// Recebe uma string e retorna duas string
func getInitials(n string) (string, string) {
	sliceN := strings.ToUpper(n)
	splitString := strings.Split(sliceN, " ")

	var initials []string

	for _, v := range splitString {
		initials = append(initials, v[:1])
	}

	if len(initials) > 1 {
		return initials[0], initials[1]
	}

	return initials[0], "_"
}

func main() {
	fn1, sn1 := getInitials("Kaio Dragon")
	fmt.Println(fn1, sn1)
}

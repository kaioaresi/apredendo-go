package main

import "fmt"

func main() {
	x := "Oi"
	y := "Bom dia"
	z := fmt.Sprint(x, y) // valor não é exibido na tela e sim salvo em uma váriavel

	fmt.Println(z)
}

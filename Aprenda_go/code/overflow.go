package main

import "fmt"

func main() {
	var i uint16 // valor maximo '65535' suportado
	i = 65535
	fmt.Println(i)
	i++ // agora vamos causar um overflow, ou seja ultrapassar o limit suportado desse tipo
	fmt.Println(i)
}

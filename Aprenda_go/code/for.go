package main

import "fmt"

func main() {
	for h := 0; h <= 12; h++ {
		fmt.Printf("Hora: %v\n\n", h)
		for m := 0; m <= 60; m++ {
			fmt.Printf("\tMinuto: %v\n", m)
			for s := 0; s <= 60; s++ {
				fmt.Printf("\t\tSegundo: %v\n", s)
			}
			fmt.Println()
		}
		fmt.Println()
	}
}

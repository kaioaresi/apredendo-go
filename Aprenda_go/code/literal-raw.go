package main

import "fmt"

func main() {
	texto_interpretado := "Oi\nOi\nOi\nOi"
	texto_literal := `Oi\nOi\nOi\nOi`
	fmt.Println(texto_interpretado)
	fmt.Println(texto_literal)
}

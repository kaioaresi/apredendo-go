package main

import "fmt"

func main() {
	valor := 100
	fmt.Printf("Valor decimal %d\n", valor)
	fmt.Printf("Valor binário %b\n", valor)
	fmt.Printf("Valor hexadecimal %#x\n", valor)
}

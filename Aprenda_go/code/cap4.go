package main

import "fmt"

func main() {
	// Para imprimir strings com formato estranho, multiplas linhas quebradas e etc, pode ser usar o "`", para esse casos
	// s := `Teste
	// 		string
	// 				estranha`
	// fmt.Printf("%v\n%v", s, s)

	// Convertendo strig em slice de byte
	s := "Hello, world!"
	sb := []byte(s)

	for _, v := range sb {
		fmt.Printf("%v - %T - %#U - %#x\n", v, v, v, v)
	}
}

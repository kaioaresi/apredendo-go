package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	var wg sync.WaitGroup
	contador := 0
	totalGoRoutine := 10
	fmt.Println("CPUs: ", runtime.NumCPU())
	fmt.Println("Goroutine: ", runtime.NumGoroutine())

	for i := 0; i < totalGoRoutine; i++ {
		go func() {
			v := contador
			runtime.Gosched() // Yield
			v++
			contador = v
			wg.Done()
		}()
		fmt.Println("Goroutine goroutine: ", runtime.NumGoroutine())
	}

	wg.Wait()
	fmt.Println("Contador final: ", contador)
	fmt.Println("Goroutine: ", runtime.NumGoroutine())
}

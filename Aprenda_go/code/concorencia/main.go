package main

import (
	"fmt"
	"runtime"
	"sync"
)

/*Criando waitGroup
-- Será utilizados nas func em 3 momento (add,wait e done)
*/

func func1(wg *sync.WaitGroup) {
	for i := 0; i < 10; i++ {
		fmt.Println("Func 1: ", i)
	}
	defer wg.Done() // aqui informamos que essa goroutime finalizou sua execução
}

func func2(wg *sync.WaitGroup) {
	for i := 0; i < 10; i++ {
		fmt.Println("Func 2: ", i)
	}

	defer wg.Done()
}

func main() {
	var wg sync.WaitGroup

	fmt.Println("Num. CPU: ", runtime.NumCPU())
	fmt.Println("Num. Goroutine: ", runtime.NumGoroutine())

	// add
	wg.Add(2)     // total de goroutines q será executada
	go func1(&wg) // goroutine - possui um `go` na frente da func()
	go func2(&wg)

	fmt.Println("Num. CPU: ", runtime.NumCPU())
	fmt.Println("Num. Goroutine: ", runtime.NumGoroutine())

	// wait
	wg.Wait() // informa a func main q deve esperar a goroutine ser executada antes de finalizar o programa

}

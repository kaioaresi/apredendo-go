package main

import "fmt"

func main() {
	y := 1
	x := y << 2 // descolando o bit de y em duas casas valor para esquerda
	z := y >> 3 // descolando o bit de y em 3 casas para direita
	fmt.Printf("%b\n", x)
	fmt.Printf("%b\n", z)
}

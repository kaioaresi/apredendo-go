package main

import "fmt"

func main() {
	// Exemplo básico
	// const (
	// 	x = iota
	// 	z = iota
	// 	y = iota
	// )

	const (
		x = iota * 2 // multiplicando os valor
		y
		_ // descartando um valor da seguencia
		z
	)
	fmt.Println(x, z, y)
}

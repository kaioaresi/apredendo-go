package main

import "fmt"

const x = 10

// declarando multiplas constantes
const (
	k = 20
	m = 30
	v = 40
)

var j int
var y float64

func main() {
	// As não tipadas só terão um tipo atribuido a elas quando forem usadas.
	y = x
	j = x
	fmt.Printf("%T - %T\n", y, j)
	fmt.Println(k, m, v)
}

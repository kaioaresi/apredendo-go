package main

import "fmt"

func main() {
	a := "e"
	b := "é"
	c := "e"

	fmt.Printf("%v, %v, %v\n", a, b, c)

	// convertedo o valor de a em byte em slice
	d := []byte(a)
	e := []byte(b)
	f := []byte(c)

	// Exibindo o valor em bytes dos slice, ascii
	fmt.Printf("%v, %v, %v\n", d, e, f)

}

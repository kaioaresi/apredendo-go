package main

import "fmt"

func main() {
	for i := 0; i < 20; i++ {
		if i%2 != 0 { // i impar
			continue // quebra a iteração sem quebrar o loop
		}
		fmt.Println(i) // exibe i quando par
	}
}

// Escreva um programa que mostre um número em decimal, binário e hexadecimal

package main

import "fmt"

func main() {
	n := 100
	fmt.Printf("Binário: %b\nDecimal: %d\nHexadecimal: %#x\n", n, n, n)
}

/*
- Utilizando iota, crie 4 constantes cujos valores sejam os próximos 4 anos.
- Demonstre estes valores.
*/

package main

import "fmt"

func main() {
	const (
		_ = 1989 + iota
		x
		y
		k
		m
	)
	fmt.Println(x, y, k, m)
}

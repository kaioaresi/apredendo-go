/* Escreva uma expressão utilizando os seguintes operadores, e atribua seus valores a váriaveis.
- ==
- <=
- >=
- !=
- >
- <
*/

package main

import "fmt"

func maior(n1 int, n2 int) {
	if n1 > n2 {
		fmt.Println("Primeiro número é maior")
	} else {
		fmt.Println("Segundo número é maior")
	}
}

func menor(n1 int, n2 int) {
	if n1 < n2 {
		fmt.Println("Primeiro número é menor")
	} else {
		fmt.Println("Primeiro número é menor")
	}
}

func maiorIgual(n1 int, n2 int) {
	if n1 >= n2 {
		if n1 == n2 {
			fmt.Println("Números são iguais")
		} else {
			fmt.Println("Primeiro número é maior")
		}
	} else {
		fmt.Println("Primeiro número é menor")
	}
}

func menorIgual(n1 int, n2 int) {
	if n1 <= n2 {
		if n1 == n2 {
			fmt.Println("Números são iguais")
		} else {
			fmt.Println("Primeiro número é maior")
		}
	} else {
		fmt.Println("Primeiro número é menor")
	}
}

func diferente(n1 int, n2 int) {
	if n1 != n2 {
		fmt.Println("Números são diferetes!")
	}
}

func maior100(n1 int, n2 int) {
	n := 100
	if n1 > n {
		fmt.Println("N1 é maior")
	} else if n2 > n {
		fmt.Println("N2 é maior")
	} else {
		fmt.Println("N1 e N2 são iguais a 100")
	}
}

func main() {
	x := 3
	y := 2
	maior(x, y)
	menor(x, y)
	maiorIgual(x, y)
	menorIgual(x, y)
	maior100(x, y)
}

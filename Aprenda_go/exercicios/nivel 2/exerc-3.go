// Crie uma constante tipada e não tipada e exiba os valores
package main

import "fmt"

type guerreiro string

const barbaro guerreiro = "Conan"

const arma = "Machado"

func main() {
	fmt.Printf("%v - %T\n", barbaro, barbaro)
	fmt.Printf("%v - %T\n", arma, arma)
}

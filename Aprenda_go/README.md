# GO

O que é golang

- Linguagem de programação open source que tem o objetivo de tornar os programadores mais produtivos
- Expressiva, consica, limpa e eficiente
- Foi criada para aproveitar ao máximo dos recursos multicore e de rede
- Rápida compilação e ao mesmo tempo trabalha com `garbage collection`
- Rápida, `estaticamente tipada`, compilada mas que ao mesmo tempo parece até uma linguagem dinamicamente tipada e interpretada
- Compilada em apenas um arquivo binário



---

# Curso Aprenda Go

## Variáveis, Valores & Tipos – 2. Hello world!

- Estrutura básica: 
    - package main.
    - func main: é aqui que tudo começa, é aqui que tudo acaba.
    - import.
- Packages:
    - Pacotes são coleções de funções pré-prontas (ou não) que você pode utilizar.
    - Notação: pacote.Identificador. Exemplo: fmt.Println()
    - Documentação: fmt.Println.
- Variáveis: "uma variável é um objeto (uma posição na memória) capaz de reter e representar um valor ou expressão."
- Variáveis não utilizadas? Não pode: _ nelas.
- ...funções variádicas.
- Lição principal: package main, func main, pacote.Identificador.

```
package main

import "fmt"

func main() {
	_, e := fmt.Println("Hello, world!") // declaração de variavel, ignorando uma das informações retornadas
	fmt.Println(e) // Função variadira, recebe qualquer quantidade de valores e entradas, a se exibida na tela
}

```

## Variáveis, Valores & Tipos – 3. Operador curto de declaração

- **:=** parece uma marmota (gopher) ou o punisher.
- Uso:
    - Tipagem automática
    - Só pode repetir se houverem variáveis novas 
    - != do assignment operator (operador de atribuição)
    - Só funciona dentro de codeblocks
- Terminologia:
    - keywords (palavras-chave) são termos reservados
    - operadores, operandos
    - statement (declaração, afirmação) → uma linha de código, uma instrução que forma uma ação, formada de expressões 
    - expressão → qualquer coisa que "produz um resultado"
    - scope (abrangência)
        - package-level scope
- Lição principal:
    - := utilizado pra criar novas variáveis, dentro de code blocks
    - = para atribuir valores a variáveis já existentes

```
package main

import "fmt"

func main() {
	// O tipo das variavies são atribuidas automaticamente
	x := 10
	y := "Teste 123"

	/* Printf, aqui estamos utilizando dois atributos %v e %T
	%v = Mostra o valor da variavel
	%T = mostra o tipo do valor
	*/

	fmt.Printf("x: %v, %T\n", x, x)

	fmt.Printf("y: %v, %T", y, y)
}
```

```
package main

import "fmt"

var x = 10 // Essa é a forma de realizar declaração de variaveis globalmente

func main() {
	y := "Teste 123"

	fmt.Printf("x: %v, %T\n", x, x)

	fmt.Printf("y: %v, %T", y, y)
}
```

## Variáveis, Valores & Tipos – 4. A palavra-chave var

- Variável declarada em um code block é undefined em outro
- Para variáveis com uma abrangência maior, package level scope, utilizamos `var`
- Funciona em qualquer lugar
- Prestar atenção: chaves, colchetes, parênteses

## Variáveis, Valores & Tipos – 5. Explorando tipos

- Tipos em Go são extremamente importantes.
- Tipos em Go são estáticos.
- Ao declarar uma variável para conter valores de um certo tipo, essa variável só poderá conter valores desse tipo.
- O tipo pode ser deduzido pelo compilador:
    - x := 10
    - var y = "a tia do batima"
- Ou pode ser declarado especificamente:
    - var w string = "isso é uma string"
    - var z int = 15
    - na declaração var z int com package scope, atribuição z = 15 no codeblock (somente)
- Tipos de dados primitivos: disponíveis na linguagem nativamente como blocos básicos de construção
    - int, string, bool
- Tipos de dados compostos: são tipos compostos de tipos primitivos, e criados pelo usuário
    - `slice`, `array`, `struct`, `map`
- O ato de definir, criar, estruturar tipos compostos chama-se composição.

## Variáveis, Valores & Tipos – 6. Valor zero

- Declaração vs. inicialização vs. atribuição de valor. Variáveis: caixas postais.
- O que é valor zero?
- Os zeros:
    - ints: 0
    - floats: 0.0
    - booleans: false
    - strings: ""
    - pointers, functions, interfaces, slices, channels, maps: nil

> - Use := sempre que possível.
> - Use var para package-level scope.


## Variáveis, Valores & Tipos – 7. O pacote fmt

- Setup: strings, ints, bools.
- Strings: interpreted string literals vs. raw string literals.
    - Rune literals.
    - Em ciência da computação, um literal é uma notação para representar um valor fixo no código fonte. 
- Format printing: documentação.
    - Grupo #1: Print → standard out
        - func Print(a ...interface{}) (n int, err error)
        - func Println(a ...interface{}) (n int, err error)
        - func Printf(format string, a ...interface{}) (n int, err error)
            - Format verbs. (%v %T)
    - Grupo #2: Print → string, pode ser usado como variável
        - func Sprint(a ...interface{}) string
        - func Sprintf(format string, a ...interface{}) string
        - func Sprintln(a ...interface{}) string
    - Grupo #3: Print → file, writer interface, e.g. arquivo ou resposta de servidor
        - func Fprint(w io.Writer, a ...interface{}) (n int, err error)
        - func Fprintf(w io.Writer, format string, a ...interface{}) (n int, err error)
        - func Fprintln(w io.Writer, a ...interface{}) (n int, err error)

```
func main() {
	texto_interpretado := "Oi\nOi\nOi\nOi"
	texto_literal := `Oi\nOi\nOi\nOi`
	fmt.Println(texto_interpretado)
	fmt.Println(texto_literal)
}
```


Sprintf


```
/*
Quando usei o Sprintf salvando dados de tipos diferentes, foi necessário tratar os tipos de dados, seguindo as opções abaixo:

- %s	the uninterpreted bytes of the string or slice
- %d  int, int8 etc.
- %t	the word true or false
*/
func main() {
	s := fmt.Sprintf("%d %s %t", x, y, z)
	fmt.Println(s)
}
```

## Variáveis, Valores & Tipos – 8. Criando seu próprio tipo

- Revisando: tipos em Go são extremamente importantes. (Veremos mais quando chegarmos em métodos e interfaces.)
- Tem uma história que Bill Kennedy dizia que se um dia fizesse uma tattoo, ela diria "type is life."
- Grande parte dos aspectos mais avançados de Go dependem quase que exclusivamente de tipos.
- Como fundação para estas ferramentas, vamos aprender a declarar nossos próprios tipos.
- Revisando: tipos são fixos. Uma vez declarada uma variável como de um certo tipo, isso é imutável.
- type hotdog int → var b hotdog (main hotdog)
- Uma variável de tipo hotdog não pode ser atribuida com o valor de uma variável tipo int, mesmo que este seja o tipo subjacente de hotdog.

```
	type batata int
	var x batata
	fmt.Printf("x: %v, %T", x, x)
```
 
## Variáveis, Valores & Tipos – 9. Conversão, não coerção

- Conversão de tipos é o que soa.
- Em Go não se diz casting, se diz conversion.
- a = int(b)
- ref/spec#Conversions

---

## Cap. 4 – Fundamentos da Programação – 1. Tipo booleano

- Agora vamos explorar os tipos de maneira mais detalhada. golang.org/ref/spec. A começar pelo bool.
- O tipo bool é um tipo binário, que só pode conter um dos dois valores: true e false. (Verdadeiro ou falso, sim ou não, zero ou um, etc.)
- Sempre que você ver operadores relacionais, o resultado da expressão será um valor booleano.
- Booleans são fundamentais nas tomadas de decisões em lógica condicional, declarações switch, declarações if, fluxo de controle, etc.
- Na prática:
    - Zero value
    - Atribuindo um valor
    - Bool como resultado de operadores relacionais

## Cap. 4 – Fundamentos da Programação – 2. Como os computadores funcionam

- Isso é importante pois daqui pra frente vamos falar de ints, bytes, e etc.
- Não é necessário um conhecimento a fundo mas é importante ter uma idéia de como as coisas funcionam por trás dos panos.
- https://docs.google.com/presentation/...
- ASCII: https://en.wikipedia.org/wiki/ASCII
- Filme: Alan Turing, The Immitation Game.


## Cap. 4 – Fundamentos da Programação – 3. Tipos numéricos

- int vs. float: Números inteiros vs. números com frações.
- golang.org/ref/spec → numeric types
- Integers:
    - Números inteiros
    - int & uint → “implementation-specific sizes”
    - Todos os tipos numéricos são distintos, exceto:
        - byte = uint8
        - rune = int32 (UTF8)
        (O código fonte da linguagem Go é sempre em UTF-8).
    - Tipos são únicos
        - Go é uma linguagem estática
        - int e int32 não são a mesma coisa
        - Para "misturá-los" é necessário conversão
    - Regra geral: use somente `int`
- Floating point:
    - Números racionais ou reais
    - Regra geral: use somente `float64`
- Na prática:
    - Defaults com :=
    - Tipagem com var
    - Dá pra colocar número com vírgula em tipo int?
    - Overflow

```golang
func main() {
	a := "e"
	b := "é"
	c := "e"

	fmt.Printf("%v, %v, %v\n", a, b, c)

	// convertedo o valor de a em byte em slice
	d := []byte(a)
	e := []byte(b)
	f := []byte(c)

	// Exibindo o valor em bytes dos slice, ascii
	fmt.Printf("%v, %v, %v\n", d, e, f)
}
```

### Overflow

- Um uint16, por exemplo, vai de 0 a 65535.
- Que acontece se a gente tentar usar 65536?
- Ou se a gente estiver em 65535 e tentar adicionar mais 1?

```golang
func main() {
	var i uint16 // valor maximo '65535' suportado
	i = 65535
	fmt.Println(i)
	i++ // agora vamos causar um overflow, ou seja ultrapassar o limit suportado desse tipo
	fmt.Println(i)
}
```

Saída do exemplo:

```output
65535 // <- Aqui apenas exibimos o valor limite suportado
0 // <- Após adicionarmos mais um número excedemos o limit suportado, fazendo com que o valor reiniciar isso é o temido `overflow`
```

## Tipo string (cadeias de caracteres)

- Strings são sequencias de bytes.
- Imutáveis.
- Uma string é um "slice of bytes" (ou, em português, uma fatia de bytes).
- Na prática:
    - %v %T
    - Raw string literals
    - Conversão para slice of bytes: []byte(x)
    - %#U, %#x

Exemplo 1:
```golang
func main() {
    // Para imprimir strings com formato estranho, multiplas linhas quebradas e etc, pode ser usar o "`", para esse casos
	s := `Teste 
			string 	
					estranha`
	fmt.Printf("%v\n%v", s, s)
}
```

Exemplo 2:

```golang
func main() {
	// Convertendo strig em slice de byte
	s := "Hello, world!"
	sb := []byte(s)

	for _, v := range sb {
		fmt.Printf("%v - %T - %#U - %#x\n", v, v, v, v)
	}
}
```

```output
72 - uint8 - U+0048 'H' - 0x48
101 - uint8 - U+0065 'e' - 0x65
108 - uint8 - U+006C 'l' - 0x6c
108 - uint8 - U+006C 'l' - 0x6c
111 - uint8 - U+006F 'o' - 0x6f
44 - uint8 - U+002C ',' - 0x2c
32 - uint8 - U+0020 ' ' - 0x20
119 - uint8 - U+0077 'w' - 0x77
111 - uint8 - U+006F 'o' - 0x6f
114 - uint8 - U+0072 'r' - 0x72
108 - uint8 - U+006C 'l' - 0x6c
100 - uint8 - U+0064 'd' - 0x64
33 - uint8 - U+0021 '!' - 0x21
```

## Sistemas numéricos

- Base-10: decimal, 0–9
- Base-2: binário, 0–1
- Base-16: hexadecimal, 0–f


String and slice of bytes (treated equivalently with these verbs):


- `%s`	the uninterpreted bytes of the string or slice
- `%q`	a double-quoted string safely escaped with Go syntax
- `%x`	base 16, lower-case, two characters per byte
- `%X`	base 16, upper-case, two characters per byte

> `#`	alternate format: add leading 0b for binary (%#b), 0 for octal (%#o),
	0x or 0X for hex (%#x or %#X); suppress 0x for %p (%#p);
	for %q, print a raw (backquoted) string if strconv.CanBackquote
	returns true;
	always print a decimal point for %e, %E, %f, %F, %g and %G;
	do not remove trailing zeros for %g and %G;
	write e.g. U+0078 'x' if the character is printable for %U (%#U).

```golang
func main() {
	valor := 100
	fmt.Printf("Valor decimal %d\n", valor)
	fmt.Printf("Valor binário %b\n", valor)
	fmt.Printf("Valor hexadecimal %#x\n", valor)
}
```

## Constantes

- São valores imutáveis.
- Podem ser tipadas ou não:
    - const oi = "Bom dia"
    - const oi string = "Bom dia"
- As não tipadas só terão um tipo atribuido a elas quando forem usadas.
    - Ex. qual o tipo de 42? int? uint? float64?
    - Ou seja, é uma flexibilidade conveniente.
- Na prática: int, float, string.
    - const x = y
    - const ( x = y )

```golang
const x = 10

var j int
var y float64

func main() {
	/* As não tipadas só terão um tipo atribuido a elas quando forem usadas.
        o Tipo de uma variavel é atribuido no momento de uma atribuição de valor
    */
	y = x
	j = x
	fmt.Printf("%T - %T", y, j)
}
```

>  declarando multiplas constantes

```golang
const x = 10

const (
	k = 20
	m = 30
	v = 40
)

func main() {
	fmt.Println(k, m, v)
}
```

## Iota

Representa valores sucessivos inteiros não tipados

```golang
func main() {
	const (
		x = iota
		z = iota
		y = iota
	)
	fmt.Println(x, z, y)
}
```

```output
0 1 2
```

- Numa declaração de constantes, o identificador iota representa números sequenciais.
- Na prática.
    - iota, iota + 1, a = iota b c, reinicia em cada const, `_`

```golang
func main() {
	const (
		x = iota * 2 // multiplicando os valor
		y
		_ // descartando um valor da seguencia
		z
	)
	fmt.Println(x, z, y)
}
```

## Deslocamento de bits

Deslocamento de bits é quando deslocamos digitos binários para a esquerda ou direita.

## Extra

Operadores aritméticos:

- Soma `+`
- Subtração `-`
- Multiplicação `*`
- Divisão `/`
- Módulo (Resto da Divisão) `%`
- Incremento `++`
- Decremento `--`

Operadores relacionais:

- Comparação `==` // usei
- Diferença `!=` // usei
- Maior `>`
- Menor `<`
- Maior ou igual `>=`
- Menor ou igual `<=` // usei


Operadores lógicos:

- Conjunção (E) `&&`
- Disjunção (Ou) `||`
- Negação `!=`

Operadores de atribuição:

- Atribuição `=`
- Soma e atribuição `+=`
- Subtração e atribuição `-=`
- Multiplicação e atribuição `*=`
- Divisão e atribuição `/=`
- Módulo e atribuição `%=`

## Receber Input

Input simples

```golang
func main() {
	var nome string
	fmt.Println("Digite seu nome: ")
	fmt.Scanln(&nome)
	fmt.Printf("Seu nome é %v\n", nome)
}
```

Input simples

```golang
func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Digite seu nome: ")
	nome, err := reader.ReadString('\n')
	if err != nil {
		log.Printf("Erro em receber input - %v", err)
	}
	fmt.Printf("Seu nome é : %v", nome)
}
```

## Gerenciamento de memoria

## Ponteiros

Quando é criado uma nova variável, o computador reserva um espaço na memoria e cada espaço tem um endereço na memoria, esses endereços de são representados como número na base hexadecimal semelhantes a esse => `0xc0000180f0`, para se descobrir um endereço de memoria, segue exemplo:

```golang
func main() {
	var idade int
	fmt.Println(&idade) // o `&` seguido de uma variavel exibe o endereço de memoria da variavel
}
```

Poteiros é uma váriavel que como toda variavel possui um endereço de memoria e dentro dela ela guarda outro endereço de memoria.

![Ponteiros](./img/ponteiros.png)

```golang
func main() {
	var idade int
	var p *int // é utilizado um `*` para representar um ponteiro, ou seja essa variavel recebe um ponteiro do tipo int

	idade = 50
	p = &idade // aqui a var `p` recebe o endereço de memoria da var `idade`

	fmt.Println(idade)
	fmt.Println(&idade)
	fmt.Println(p)
	fmt.Println(&p)
}
```

Update valor de referencia do ponteiro

```golang
func main() {
	var idade int
	var p *int // é utilizado um `*` para representar um ponteiro, ou seja essa variavel recebe um ponteiro do tipo int

	idade = 50
	p = &idade // aqui a var `p` recebe o endereço de memoria da var `idade`

	fmt.Printf("Valor de idade => %v, endereço da memoria => %v\n", idade, &idade)
	fmt.Printf("Vaor de p => %v, endereço de p %v\n", p, &p)
	fmt.Printf("Valor que a referencia que p está apontando, ou seja valor de idade %v\n", *p) // exibe o valor da variavel que o ponteiro está apontando

	*p = 60 // alterando o valor de idade atravez do ponteiro
	fmt.Printf("Update de idade %v\n", idade)
	fmt.Printf("Novo valor de p => %v\n", *p)
}
```

Aplicação no mundo real

```golang
var idade int = 50

// recebe um ponteiro o altera e retorna um int
func dobro(valor *int) int {
	*valor = *valor * 2
	return *valor
}

// Apenas exibe o valor e não o altera
func triplo(valor int) int {
	valor = valor * 3
	return valor
}

func main() {
	fmt.Printf("Idade antes da alteração => %v\n", idade)
	fmt.Printf("Triplicando o valor de idade => %v\n", triplo(idade))
	fmt.Printf("Idade depois do triplo => %v\n", idade)
	fmt.Println()
	fmt.Printf("Idade antes da alteração => %v\n", idade)
	fmt.Printf("Dobrando valor de idade => %v\n", dobro(&idade)) // como a func dobro recebe um ponteiro, para que o valor será alterado é preciso passar o endereço de memoria do valor
	fmt.Printf("Idade depois do dobro alterações => %v\n", idade)
}
```

## Cap. 6 - Fluxo de Controle

### 1 - Entendendo fluxo de controle

- Computadores lêem programas de uma certa maneira, do mesmo jeito que nós lemos livros, por exemplo, de uma certa maneira.
- Quando nós ocidentais lemos livros, lemos da frente pra trás, da esquerda pra direito, de cima pra baixo.
- Computadores lêem de cima pra baixo.
- Ou seja, sua leitura é sequencial. Isso chama-se fluxo de controle sequencial.
- Alem do fluxo de controle sequencial, há duas declarações que podem afetar como o computador lê o código:
    - Uma delas é o fluxo de controle de repetição (loop). Nesse caso, o computador vai repetir a leitura de um mesmo código de uma maneira específica. O fluxo de controle de repetição tambem é conhecido como fluxo de controle iterativo.
    - E o outro é o fluxo de controle condicional, ou fluxo de controle de seleção. Nesse caso o computador encontra uma condição e, através de uma declaração if ou switch, toma um curso ou outro dependendo dessa condição.
- Ou seja, há três tipos de fluxo de controle: sequencial, de repetição e condicional.

- Nesse capítulo:
    - Sequencial
    - Iterativo (loop)
        - for: inicialização, condição, pós
        - for: hierarquicamente
        - for: condição ("while")
        - for: ...ever?
        - for: break
        - for: continue
    - Condicional
        - declarações switch/case/default
            - não há fall-through por padrão
            - criando fall-through
            - default
            - múltiplos casos
            - casos podem ser expressões
                - se resultarem em true, rodam
            - tipo
        - if
            - bool
            - o operador "!"
            - declaração de inicialização
            - if, else
            - if, else if, else
            - if, else if, else if, ..., else 
	- Repetição hierárquica
	- Exemplos: relógio, calendário
	- For: break
	- For: continue


// TODO: Estudar:
	- Goroutine
	- canais
	- Padrões de projeto
	- Designe de software


## Cap. 12 – Funções – 5. Interfaces & polimorfismo

- Em Go, valores podem ter mais que um tipo.
- Uma interface permite que um valor tenha mais que um tipo.
- Declaração: keyword identifier type → type x interface
- Após declarar a interface, deve-se definir os métodos necessários para implementar essa interface.
- Se um tipo possuir todos os métodos necessários (que, no caso da interface{}, pode ser nenhum) então esse tipo implicitamente implementa a interface.
- Esse tipo será o seu tipo e também o tipo da interface.

- Exemplos:
    - Os tipos profissão1 e profissão2 contem o tipo pessoa
    - Cada um tem seu método oibomdia()*, e podem dar oi utilizando *pessoa.oibomdia()
    - Implementam a interface gente
    - Ambos podem acessar o função serhumano() que chama o método oibomdia() de cada gente
    - Tambem podemos no método serhumano() tomar ações diferentes dependendo do tipo:
        switch pessoa.(type) { case profissão1: fmt.Println(h.(profissão1).valorquesóexisteemprofissão1) [...] }* 

## Cap. 18 – Concorrência – 1. Concorrência vs. paralelismo

- Concorrência é uma maneira de pensar, é um jeito de organizar um programa, isso está relacionado a arquitetura computacional ao qual será executado o programa, caso contrario o mesmo será executado de forma subsequente e não paralela.
- Paralelismo é quando temos códigos concorentes sendo executados em um sistema que possuí vários processadores.

- Fun facts: 
    - O primeiro CPU dual core "popular" veio em 2006
    - Em 2007 o Google começou a criar a linguagem Go para utilizar essa vantagem
    - Go foi a primeira linguagem criada com multi-cores em mente
    - C, C++, C#, Java, JavaScript, Python, etc., foram todas criadas antes de 2006
    - Ou seja, Go tem uma abordagem única (fácil!) para este tópico
- E qual a diferença entre concorrência e paralelismo?

## Cap. 18 – Concorrência – 2. Goroutines & WaitGroups

- O código abaixo é linear. Como fazer as duas funções rodarem concorrentemente?
    - https://play.golang.org/p/XP-ZMeHUk4
- Goroutines!
- O que são goroutines? São "threads."
- O que são threads? [WP](https://pt.wikipedia.org/wiki/Thread_...)
- Na prática: go func.
- Exemplo: código termina antes da go func executar.
- Ou seja, precisamos de uma maneira pra "sincronizar" isso.
- Ah, mas então... não.
- Qualé então? sync.WaitGroup:
- Um WaitGroup serve para esperar que uma coleção de goroutines termine sua execução.
    - func Add: "Quantas goroutines?"
    - func Done: "Deu!"
    - func Wait: "Espera todo mundo terminar."
- Ah, mas então... sim!
- Só pra ver: runtime.NumCPU() & runtime.NumGoroutine()


```golang
import (
	"fmt"
	"runtime"
	"sync"
)

/*Criando waitGroup
-- Será utilizados nas func em 3 momento (add,wait e done)
*/

func func1(wg *sync.WaitGroup) {
	for i := 0; i < 10; i++ {
		fmt.Println("Func 1: ", i)
	}
	defer wg.Done() // aqui informamos que essa goroutime finalizou sua execução
}

func func2(wg *sync.WaitGroup) {
	for i := 0; i < 10; i++ {
		fmt.Println("Func 2: ", i)
	}

	defer wg.Done()
}

func main() {
	var wg sync.WaitGroup

	fmt.Println("Num. CPU: ", runtime.NumCPU())
	fmt.Println("Num. Goroutine: ", runtime.NumGoroutine())

	// add
	wg.Add(2)     // total de goroutines q será executada
	go func1(&wg) // goroutine - possui um `go` na frente da func()
	go func2(&wg)

	fmt.Println("Num. CPU: ", runtime.NumCPU())
	fmt.Println("Num. Goroutine: ", runtime.NumGoroutine())

	// wait
	wg.Wait() // informa a func main q deve esperar a goroutine ser executada antes de finalizar o programa

}
```

##  Cap. 18 – Concorrência – 3. Discussão: Condição de corrida


- Agora vamos dar um mergulho na documentação:
    - https://go.dev/doc/effective_go#concurrency
    - https://pt.wikipedia.org/wiki/Multiplexador
    - O que é yield? runtime.Gosched()
- Race condition: 

```
        Função 1       var     Função 2
         Lendo: 0   →   0
         Yield          0   →   Lendo: 0
         var++: 1               Yield
         Grava: 1   →   1       var++: 1
                        1   ←   Grava: 1
         Lendo: 1   ←   1
         Yield          1   →   Lendo: 1
         var++: 2               Yield
         Grava: 2   →   2       var++: 2
                        2   ←   Grava: 2
```

- E é por isso que vamos ver mutex, atomic e, por fim, channels.

![Multiplexador](../img/300px-Telephony_multiplexer_system.gif)

- Aqui vamos replicar a race condition mencionada no artigo anterior.
    - `time.Sleep(time.Second)` vs. `runtime.Gosched()`
- go help → go help build → go run -race main.go

## Cap. 21 – Canais – 1. Entendendo canais

- Eles nos permitem trasmitir valores entre goroutines.
- Servem pra coordenar, sincronizar, orquestrar, e buffering.
- Na prática:
    - make(chan type, b)
- Canais bloqueiam:
    - Eles são como corredores em uma corrida de revezamento
    - Eles tem que "passar o bastão" de maneira sincronizada
    - Se um corredor tentar passar o bastão pro próximo, mas o próximo corredor não estiver lá...
    - Ou se um corredor ficar esperando receber o bastão, mas ninguem entregar...
    - ...não dá certo.
- Exemplos:
    - Poe um valor num canal e faz um print. Block.
        - Código acima com goroutine.
        - Ou com buffer. Via de regra: má idéia; é legal em certas situações, mas em geral é melhor sempre passar o bastão de maneira sincronizada.
- Interessante: ref/spec → types


















---

# Preferencias

- [Doc Golang](https://go.dev/doc/)
- [Golang](https://go.dev/)
- [Go Playground](https://go.dev/play/)
- [ASCII table](https://web.fe.up.pt/~ee96100/projecto/Tabela%20ascii.htm)
- [iota: Elegant Constants in Golang](https://splice.com/blog/iota-elegant-constants-golang/)
- [Bit Hacking with Go](https://medium.com/learning-the-go-programming-language/bit-hacking-with-go-e0acee258827)
- [Input golang](https://www.geeksforgeeks.org/how-to-take-input-from-the-user-in-golang/)
- [Reading console input](https://tutorialedge.net/golang/reading-console-input-golang/)
- [Ponteiros e endereços de memória com a linguagem Go (golang)](https://www.youtube.com/watch?v=Ip1VpLxNOvQ)
package main

import (
	"fmt"
)

func soma(x, y int) int {
	return x + y
}

func somaIntFloatString[V int | float64 | string](x, y V) V {
	return x + y
}

func main() {
	fmt.Println("Generics")
	fmt.Println(soma(1, 2))
	fmt.Println(somaIntFloatString(1.0, 2.5))
	fmt.Println(somaIntFloatString(1, 5))
	fmt.Printf(somaIntFloatString("Teste", "12313213"))
}

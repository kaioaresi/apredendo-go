package main

import (
	"context"
	"flag"
	"fmt"
	"path/filepath"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

func msgFormat(totalPods, totalNodes int) string {
	msg := fmt.Sprintf("+----------------+\n")
	msg += fmt.Sprintf("Report cluster\n")
	msg += fmt.Sprintf("+----------------+\n")
	msg += fmt.Sprintf("Total pods: %v\n", totalPods)
	msg += fmt.Sprintf("Total Nodes: %v\n", totalNodes)
	return msg
}

// func podsStatus(pods) {
// 	for _, pod := range totalPods.Items {
// 		podName := pod.Name
// 		podStatus := pod.Status.Phase

// 		if podStatus != "Running" {
// 			fmt.Printf("Pod %s não está running", podName)
// 		}

// 		fmt.Printf("Pod Name: %s \t Status: %s\n", podName, podStatus)
// 	}
// }

func main() {
	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}

	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)

	if err != nil {
		panic(err.Error())
	}

	// List pods
	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})

	if err != nil {
		panic(err.Error())
	}

	// List Nodes
	nodes, err := clientset.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})

	if err != nil {
		panic(err.Error())
	}

	totalPods := len(pods.Items)
	totalNodes := len(nodes.Items)

	msg := msgFormat(totalPods, totalNodes)

	fmt.Println(msg)
}

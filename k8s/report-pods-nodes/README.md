# Build

Mac

```bash
GOOS=darwin GOARCH=amd64 go build -o report-k8s-mac
```


# Referencias

- [Pod lifecycle](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/)
- [kubectl client go part](http://yuezhizizhang.github.io/kubernetes/kubectl/client-go/2020/05/13/kubectl-client-go-part-2.html)
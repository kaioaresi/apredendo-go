# Testes

## Fundamentos de teste

### O que é teste ?

O teste de software é a maneira de avaliar a qualidade de software e reduzir o risco de falha do software em operação.

Existem alguns tipos de testes:

- Planejamento de teste
- Modelagem e implementação
- Relatórios de progresso e resultados de testes e avaliação de qualidade de um objeto de teste

Alguns testes envolvem a execução de um componete ou sistema que está sendo testado, sendo eesse teste chamados de `teste dinânimos`. Outros testes não envolvem a execução do componente ou sistema que está sendo testado, sendo chamados de `testes estático`.

Em geral testes não são apenas para verificação de requisitos e sim em verificar se o sistema atende aos requisitos especificados e se atenderá às necessidades do `usuário` e `stakeholders`.

### Objetivos típicos do teste

- Evitar defeitos, avaliar os produtos de trabalho, como requisitos, histórias de usuários,
modelagem e código;
- Verificar se todos os requisitos especificados foram cumpridos;
- Verificar se o objeto de teste está completo e validar se funciona como os usuários e
`stakeholders` esperam;
- Criar confiança no nível de qualidade do objeto de teste;
- Encontrar defeitos e falhas reduz o nível de risco de qualidade inadequada do software;
- Fornecer informações suficientes aos `stakeholders` para que tomem decisões especialmente
em relação ao nível de qualidade do objeto de teste;
- Cumprir os requisitos ou normas contratuais, legais ou regulamentares, ou verificar a
conformidade do objeto de teste com esses requisitos ou normas

> Os objetivos do teste podem variar, dependendo do contexto do componente ou sistema que está sendo testado.

### Teste e depuração de código

Teste e depuração de código são diferentes. A `execução dos testes` pode mostrar falhas causadas
por defeitos no software. A `depuração de código` é a atividade de desenvolvimento que localiza,
analisa e corrige esses defeitos. [ISO29119-1]

### Por que o teste é necessário?

Testes rigorosos de componentes e sistemas e sua documentação associada podem ajudar a reduzir
o risco de falhas durante a operação. Quando defeitos são detectados e posteriormente corrigidos,
há contribuição para a qualidade dos componentes ou sistemas. Além disso, o teste de software
também é necessário para atender aos requisitos contratuais ou legais ou aos padrões específicos
do setor.

### Erros, defeitos e falhas

Uma pessoa pode cometer um erro (engano), que pode levar à introdução de um defeito (falha ou
bug) no código do software ou em algum outro produto de trabalho relacionado. Um erro que leva
à introdução de um defeito em um produto de trabalho pode acionar outro erro que leva à
introdução de um defeito em um outro produto de trabalho relacionado.

Erros podem ocorrer por vários motivos, como:

- Pressão do tempo;
- Falha humana;
- Participantes do projeto inexperientes ou insuficientemente qualificados;
- Falta de comunicação entre os participantes do projeto, incluindo falta de comunicação sobre
os requisitos e a modelagem;
- Complexidade do código, modelagem, arquitetura, o problema a ser resolvido ou as
tecnologias utilizadas;
- Mal-entendidos sobre interfaces intra-sistema e entre sistemas, especialmente quando tais
interações são em grande número;
- Tecnologias novas, ou desconhecidas

Nem todos os resultados inesperados de teste são considerados falhas. `Falsos positivos` podem
ocorrer devido a erros na forma como os testes foram executados ou devido a defeitos nos dados
de teste, no ambiente de teste ou em outro testware ou por outros motivos. A situação inversa
também pode ocorrer, onde erros ou defeitos similares levam a `falsos negativos`. `Falsos negativos`
são testes que não detectam defeitos que deveriam ser detectados; `falsos positivos `são relatados
como defeitos, mas na verdade não são defeitos.

### Os 7 princípios de testes

1. O teste mostra a presença de defeitos e não a sua ausência
O teste reduz a probabilidade de defeitos não descobertos permanecerem no software, mas, mesmo
se nenhum defeito for encontrado, o teste não é uma prova de correção.

2. Testes exaustivos são impossíveis
Testar tudo (todas as combinações de entradas e pré-condições) não é viável, exceto em casos
triviais. Em vez de tentar testar exaustivamente, a análise de risco, as técnicas de teste e as
prioridades devem ser usadas para concentrar os esforços de teste.

3. O teste inicial economiza tempo e dinheiro
Para encontrar antecipadamente os defeitos, as atividades de teste estático e dinâmico devem iniciar
o mais cedo possível no ciclo de vida de desenvolvimento de software. O teste inicial é por vezes
referido como shift left. O teste no início do ciclo de vida de desenvolvimento de software ajuda a
reduzir ou eliminar alterações dispendiosas (ver capítulo 3.1).

4. Defeitos se agrupam
Um pequeno número de módulos geralmente contém a maioria dos defeitos descobertos durante
o teste de pré-lançamento ou é responsável pela maioria das falhas operacionais. Agrupamento de
defeitos previstos e os agrupamentos de defeitos observados reais em teste ou produção, são uma
entrada importante em uma análise de risco usada para focar o esforço de teste (como mencionado
no princípio 2).

5. Cuidado com o paradoxo do pesticida
Se os mesmos testes forem repetidos várias vezes, esses testes não encontrarão novos defeitos.
Para detectar novos defeitos, os testes existentes e os dados de teste podem precisar ser alterados
e novos testes precisam ser gravados. (Testes não são mais eficazes em encontrar defeitos, assim
como pesticidas não são mais eficazes em matar insetos depois de um tempo.) Em alguns casos,
como o teste de regressão automatizado, o paradoxo do pesticida tem um resultado benéfico, que
é o número relativamente baixo de defeitos de regressão.

6. O teste depende do contexto
O teste é feito de forma diferente em diferentes contextos. Por exemplo, o software de controle
industrial de segurança crítica é testado de forma diferente de um aplicativo móvel de comércio
eletrônico. Como outro exemplo, o teste em um projeto ágil é feito de forma diferente do que o teste
em um projeto de ciclo de vida sequencial.

7. Ausência de erros é uma ilusão
Algumas organizações esperam que os testadores possam executar todos os testes possíveis e
encontrar todos os defeitos possíveis, mas os princípios 2 e 1, respectivamente, nos dizem que isso
é impossível. Além disso, é uma ilusão (isto é, uma crença equivocada) esperar que apenas encontrar
e corrigir muitos defeitos assegure o sucesso de um sistema. Por exemplo, testar exaustivamente
todos os requisitos especificados e corrigir todos os defeitos encontrados ainda pode produzir um
sistema difícil de usar, que não atenda às necessidades e expectativas dos usuários ou que seja
inferior em comparação com outros sistemas concorrentes.

### Processo de teste no contexto

Alguns fatores contextuais que influenciam o processo de teste de uma organização:
- Modelo de ciclo de vida de desenvolvimento de software e metodologias de projeto utilizados;
- Níveis de teste e tipos de teste considerados;
- Riscos de produto e projeto;
- Domínio do negócio;
- Algumas restrições operacionais:
    - Orçamentos e recursos;
    - Escalas de tempo;
    - Complexidade;
    - Requisitos contratuais e regulamentares.
- Políticas e práticas organizacionais;
- Normas internas e externas necessárias

// TODO: parei na pagina 21 - Modelagem de teste


---

## As 8 leis de lehman foram o manifesto do seculo xx


1. **Mudança contínua** - Um software deve ser continuamente adaptado, senão torna-se aos poucos, cada vez menos satisfatório. A cada alteração no ambiente em que ele roda que exija nele melhorias, não fazê-las o tornarão progressivamente menos satisfatório naquilo para o que foi construído.

> Qualquer software utilizado no mundo real precisa se adaptar ou vai se tornar cada vez mais obsoleto.

2. **Complexidade crescente** - Se não forem tomadas medidas para reduzir ou manter a complexidade de um software, conforme ele é alterado sua complexidade irá aumentar progressivamente. Deve haver um esforço para reduzir a complexidade final de um sistema enquanto este recebe alterações.

> Enquanto o softwate evolui, sua complexidade aumenta. A não ser que um esforço seja investido para reduzi-la.

3. **Auto regulação** - A curva pertinente ao processo de evolução de um software em relação a seus atributos e processos são auto reguláveis e próximos a uma curva normal, subindo até um teto, quando começa a diminuir.

4. **Conservação da estabilidade organizacional** - A velocidade de atividade global efetiva de um software em evolução deverá se manter invariável durante todo o ciclo de vida deste produto. O mix que é levado em consideração para as tomadas de decisão que levam a evolução de um software tendem a ser constantes.

5. **Conservação de familiaridade** - Durante a vida útil de um software em evolução, a taxa de mudanças tende a ser proporcional ao domínio que a equipe detém. A taxa de evolução de um software está intimamente ligado ao grau de familiaridade dentre os profissionais que o mantém.

6. **Crescimento Contínuo** - Todo software deve ter o conteúdo funcional continuamente ampliado durante seu ciclo de vida para manter a satisfação dos seus usuários. O projeto inicial não consegue incluir absolutamente tudo o necessário e progressivamente precisará ser aumentado.

7. **Qualidade diminuindo** - Os softwares desenvolvidos para resolver problemas do mundo real se depreciam progressivamente se eles não receberem as mudanças necessárias para adaptar-se ao que acontece em seu ambiente operacional durante todo o tempo de seu ciclo de vida útil.

8. **Sistema de feedback** - Os processos de manutenção e evolução de um software refletem sistemas de feedback em múltiplos níveis, loops e agentes e devem ser assim tratados para manter-se significativos.


### Refatoração

Existem diversas facetas na engenharia de software que mantêm um software maleável, como:

- Capacitação do desenvolvimento
- Em termos gerais, código "bom". Separação sensível de responsabilidades, etc
- Habilidades de comunicação
- Arquitetura
- Observabilidade
- Implantabilidade
- Testes automatizados
- Retornos de feedback


---

# Referencias

- [Escrevendo testes unitarios](https://aprendagolang.com.br/2021/11/03/escrevendo-testes-unitarios/)
- [Fundamentos de teste](https://bcr.bstqb.org.br/docs/syllabus_ctfl_3.1br.pdf)
- [As 8 leis de lehman foram o manifesto do seculo xx](https://www.baguete.com.br/colunas/jorge-horacio-audy/17/04/2014/as-8-leis-de-lehman-foram-o-manifesto-do-seculo-xx)
- [Aprenda go com testes](https://larien.gitbook.io/aprenda-go-com-testes/meta/motivacao)

package soma

// Soma - soma valores e retorna o total da soma
func Soma(valores ...int) (total int) {
	for _, valor := range valores {
		total += valor
	}

	return
}

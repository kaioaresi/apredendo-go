package main

import "fmt"

type Animal struct {
	Patas int
	Voa   bool
	Nada  bool
}

type Dog struct {
	Patas int
	Late  string
}

func (a *Animal) Dog() string {
	dadosDog := Dog{Patas: a.Patas, Late: "Au! Au!"}

}

func main() {
	dog := Animal{}

	fmt.Println(dog.Dog)
}

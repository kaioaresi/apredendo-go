package testesimples

import "testing"

func Test_exibeNome(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{name: "Caminho feliz", s: "Kaio", want: "Kaio"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := exibeNome(tt.s); got != tt.want {
				t.Errorf("exibeNome() = %v, want %v", got, tt.want)
			}
		})
	}
}

package main

import "fmt"

// Veiculo - todo veiculo tem metodo combustivel
type Veiculo interface {
	combustivel()
}

type Aviao struct {
	Fabricante  string
	Modelo      string
	Combustivel string
}
type Barco struct {
	Fabricante  string
	Modelo      string
	Combustivel string
}

type Comprador struct {
	Nome string
	Veiculo
}

// Metodos
func (a Aviao) combustivel() {
	fmt.Println("Combustivel:", a.Combustivel)
}

func (b Barco) combustivel() {
	fmt.Println("Combustivel:", b.Combustivel)
}

func main() {
	f22 := Aviao{
		Fabricante:  "US arm",
		Modelo:      "F22 A",
		Combustivel: "Gasolina de jato",
	}
	barcoMilitar := Barco{
		Fabricante:  "US arm",
		Modelo:      "Encouraçado",
		Combustivel: "Querozene",
	}

	garen := Comprador{
		Nome:    "Garen",
		Veiculo: f22,
	}

	lux := Comprador{
		Nome:    "Luxana",
		Veiculo: barcoMilitar,
	}

	garen.combustivel()
	lux.combustivel()
}

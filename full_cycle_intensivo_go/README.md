## Install Golang

```
sudo apt update && sudo apt install golang

go version

go env
```


## Running code

```
go run <path>/file.go
```

## Build

```
go build // execute na raiz do path
```

## Install

Após o build do pacote, é possivel fazer o install do binario gerado

```
go install
```

## Imprimir `fmt`

```
package main

import "fmt"

func main() {
	fmt.Println("Hello, world!")
}
```

> Existem outras opções de print, é ideial ler a doc para saber as demais [opções](https://pkg.go.dev/fmt)

## Declaração de variável

1. Usando `var`, seu uso é para escopos globais

```
var nome string = "Teste 123"
```

2. Criado uma var dentro de um block

```
func main() {
    nome := "Teste 123"
	fmt.Println(nome)
}
```

> Observação: você pode ou não declarar o tipo da variável, a linguagem reconhece automaticamente o tipo caso não declarado.

## Tipos de dados

Existem vários tipos de dados, porém os mais basícos são:

- int
- float
- bool
- string
- arrays
- slice

Exemplo:

Array (padrão)

> Ao criar um array, deve-se passar o tamanho e o tipo

```
	x := [10]int{1, 2, 3, 4, 5}
	fmt.Println(x[0])
```

Slice

```
	// Slice tamanho fixo
	slice := make([]int, 5) // criando o slice
	slice[0] = 1            // atribuindo valor
	slice[1] = 2            // atribuindo valor
	slice[2] = 3            // atribuindo valor
	slice[3] = 4            // atribuindo valor
	slice[4] = 5            // atribuindo valor
	fmt.Println(slice, "Slice com 5 posições")

	// Slice infinito
	slice = append(slice, 5, 4, 3, 2, 1)
	fmt.Println(slice, "Slice infinito
```

Struct

```
type Car struct {
	CarName string
	CarYear int
}

func main() {
	car1 := Car{
		CarName: "Ford Ka",
		CarYear: 2015,
	}

	fmt.Println(car1.CarName)
}
```

Struct to json with tags


```
import (
	"encoding/json"
	"fmt"
)

type Car struct {
	CarName string `json:"car"`  // tags a ser exibido quando convertido em json
	CarYear int    `json:"year"` // tags a ser exibido quando convertido em json
}

// Criado metodo para struct Car
func (c Car) drive() {
	fmt.Println("Andou!")
}

func main() {
	car1 := Car{
		CarName: "Ford Ka",
		CarYear: 2015,
	}

	result, _ := json.Marshal(car1) // Convert para slibe byte
	fmt.Println(string(result))     // convertendo para string o slice byte
}

```


Json to struc

```
type Car struct {
	CarName string `json:"car"`  // tags a ser exibido quando convertido em json
	CarYear int    `json:"year"` // tags a ser exibido quando convertido em json
}

func main() {
	j := []byte(`{"car":"BMW", "year":"2020"}`)
	var car Car
	json.Unmarshal(j, &car)
	fmt.Println(car.CarName)
}
```

## Entrada de dados


## Request http

Exemplo 1:

```
package main

import (
	"fmt"
	"net/http"
)

func main() {
	response, erro := http.Get("https://google.com")
	fmt.Println(response.Body)
	fmt.Println(erro)
}
```

Observação: O metodo da função `http` retorna dois valores sem um response e um erro, caso você não queira receber uma das duas funções, você pode passa na posição da var o [blank identifier](https://golangdocs.com/blank-identifier-in-golang) `_` , que aquele valor será ignorado.

Exemplo 2:

```
func main() {
	response, _ := http.Get("https://google.com")
	fmt.Println(response.Body)
}
```

## Funções

Exemplo: 

```
func main() {
	a, b, err := nome("Kaio", 50)

	if err != nil {
		panic("Panico")
	}

	fmt.Println(a, b)
}


func nome(a string, b int) (string, int, error) {
	// recebe duas vars a string e b int e retorna duas var string, int e um erro se ocorrer
	if b > 100 {
		return "", 0, errors.New("Erro generico!")
	}
	return a, b, nil
}

```

## Ponteiros

É uma forma de acessar posição de memoria de uma váriavel, ai inves de acessar/copiar a váriavel

```
var x int = 10

func main() {
	fmt.Println(x)
	fmt.Println(&x) // Exibindo a posição da var x
}

```

## Interfaces

---


--- 

# Referencias

- [Intensivo Golang: O mínimo que você precisa saber](https://www.youtube.com/watch?v=ye6vpu4tCaE2)
- [how to use struct tags in go](https://www.digitalocean.com/community/tutorials/how-to-use-struct-tags-in-go-pt)

package main

import "fmt"

func main() {
	// Array padrao
	x := [10]int{1, 2, 3, 4, 5}
	fmt.Println(x)

	// Slice tamanho fixo
	slice := make([]int, 5) // criando o slice
	slice[0] = 1            // atribuindo valor
	slice[1] = 2            // atribuindo valor
	slice[2] = 3            // atribuindo valor
	slice[3] = 4            // atribuindo valor
	slice[4] = 5            // atribuindo valor
	fmt.Println(slice, "Slice com 5 posições")

	// Slice infinito
	slice = append(slice, 5, 4, 3, 2, 1)
	fmt.Println(slice, "Slice infinito")
}

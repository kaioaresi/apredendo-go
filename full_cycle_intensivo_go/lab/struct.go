package main

import (
	"encoding/json"
	"fmt"
)

type Car struct {
	CarName string `json:"car"`  // tags a ser exibido quando convertido em json
	CarYear int    `json:"year"` // tags a ser exibido quando convertido em json
}

// Criado metodo para struct Car
// func (c Car) drive() {
// 	fmt.Println("Andou!")
// }

// func main() {
// 	car1 := Car{
// 		CarName: "Ford Ka",
// 		CarYear: 2015,
// 	}

// 	result, _ := json.Marshal(car1) // Convert para slibe byte
// 	fmt.Println(string(result))     // convertendo para string o slice byte
// }

func main() {
	j := []byte(`{"car":"BMW", "year":"2020"}`)
	var car Car
	json.Unmarshal(j, &car)
	fmt.Println(car.CarName)
}

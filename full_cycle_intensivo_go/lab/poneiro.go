package main

import "fmt"

var x int = 10

func main() {

	fmt.Println(x)
	fmt.Println(&x) // Exibindo a posição da var x

}

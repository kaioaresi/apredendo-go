package main

import (
	"errors"
	"fmt"
)

func main() {
	a, b, err := nome("Kaio", 50)

	if err != nil {
		panic("Panico")
	}

	fmt.Println(a, b)
}

func nome(a string, b int) (string, int, error) {
	// recebe duas vars a string e b int e retorna duas var string, int e um erro se ocorrer
	if b > 100 {
		return "", 0, errors.New("Erro generico!")
	}
	return a, b, nil
}

package main

type vehicle interface {
	start() string
}

type Car struct {
	CarName string `json:"car"`
	CarYear int    `json:"year"`
}

func (c car) start() string {
	return "Startou"
}

func exemplo(car vehicle) {

}

func main() {
	car = Car{
		CarName: "Ferrari",
		CarYear: 2021,
	}
	exemplo(car)
}

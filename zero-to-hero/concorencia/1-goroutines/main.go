package main

import (
	"fmt"
	"time"
)

func escreve(t string) {
	for {
		fmt.Println(t)
		time.Sleep(time.Second)
	}
}

// Concorrência != paralelismo
func main() {
	go escreve("Teste 123") // gorountine - começe a execução mas n espere termina e já siga o script
	escreve("Teste k")      // execução normal
}

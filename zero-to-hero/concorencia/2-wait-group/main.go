package main

import (
	"fmt"
	"sync"
	"time"
)

func escreve(t string) {
	for i := 0; i < 5; i++ {
		fmt.Println(t)
		time.Sleep(time.Second)
	}
}

func main() {
	var waitGroup sync.WaitGroup

	waitGroup.Add(2) // quantidade de goroutines que ele precisa esperar terminar, antes de finalizar o programa

	// goroutine 1
	go func() {
		escreve("Teste 123")
		waitGroup.Done() // avisa que uma goroutine foi finalizada, remove uma goroutine do contado
	}()
	// goroutine 2
	go func() {
		escreve("Teste k")
		waitGroup.Done()
	}()

	waitGroup.Wait() // informa para func main, para ele esperar ambas goroutine finalizar
}

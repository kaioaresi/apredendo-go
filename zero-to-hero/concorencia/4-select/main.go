package main

import (
	"fmt"
	"time"
)

func main() {
	canal1, canal2 := make(chan string), make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			canal1 <- "Canal 1" // enviando mensagem para o canal 1
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			canal2 <- "Canal 2" // enviando mensagem para o canal 1
		}
	}()

	for {

		select {
		case msg1 := <-canal1: // caso canal 1 pronto para receber valor, execute a proxima linha é um print
			fmt.Println(msg1)
		case msg2 := <-canal2: // caso canal 2 pronto para receber valor, execute a proxima linha é um print
			fmt.Println(msg2)
		}
	}

}

package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Retorna um canal tipo string
func escreve(texto string) <-chan string {
	canal := make(chan string)
	go func() {
		for {
			canal <- fmt.Sprintf("Valor recebido: %s", texto)
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(2000)))
		}
	}()

	return canal
}

// Func que recebe dois canais e retorna um canal
func multiplexar(canal1, canal2 <-chan string) <-chan string {
	canalSaida := make(chan string)

	// recebendo entrada de ambos canais e convertendo para apenas um canal, tudo feito dentro de uma func anonima
	go func() {
		for {
			select {
			case msg := <-canal1:
				canalSaida <- msg
			case msg := <-canal2:
				canalSaida <- msg
			}
		}
	}()

	return canalSaida
}

func main() {

	canal := multiplexar(escreve("Teste k"), escreve("Programando em GO!"))
	for i := 0; i < 10; i++ {
		fmt.Println(<-canal)
	}
}

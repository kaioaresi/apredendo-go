package main

import (
	"fmt"
	"time"
)

// Retorna um canal tipo string
func escreve(texto string) <-chan string {
	canal := make(chan string)
	go func() {
		for {
			canal <- fmt.Sprintf("Valor recebido: %s", texto)
			time.Sleep(time.Millisecond * 500)
		}
	}()

	return canal
}

func main() {
	canal := escreve("Teste k")

	// Aqui eu controle a quantidade de vezes que a mensagem será impressa na tela
	for i := 0; i < 10; i++ {
		fmt.Println(<-canal)
	}
}

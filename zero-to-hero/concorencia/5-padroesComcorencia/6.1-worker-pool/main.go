package main

import "fmt"

// Função recursiva
func fibonati(posicao int) int {
	if posicao <= 1 {
		return posicao
	}

	return fibonati(posicao-2) + fibonati(posicao-1)
}

// Especificando que o canal tarefas só recebe dados `<-` e o canal resultado só recebe dados, ps a posição é importante, siga o exemplo
func worker(tarefas <-chan int, resultados chan<- int) {
	for numero := range tarefas {
		resultados <- fibonati(numero) // para cada entrada será envio o valor para o canal resultado
	}
}

func main() {
	tarefas := make(chan int, 45)
	resultados := make(chan int, 45)

	// Aumentando numeros de rotinas, para aumentar a velocidade de processamento
	go worker(tarefas, resultados)
	go worker(tarefas, resultados)
	go worker(tarefas, resultados)
	go worker(tarefas, resultados)

	for i := 0; i < 45; i++ {
		tarefas <- i
	}

	close(tarefas) // fechando canal

	for i := 0; i < 45; i++ {
		resultado := <-resultados
		fmt.Println(resultado)
	}

}

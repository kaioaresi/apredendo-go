package main

import "fmt"

func main() {
	canal := make(chan string, 2) // o número é um buffer do canal, informando a capacidade que é de duas entradas, isso evita o `deadlock`

	// Podemos enviar apenas duas mensagens
	canal <- "Mensagem 1"
	canal <- "Mensagem 2"

	// Podemos receber também apenas duas mensagens
	mensage := <-canal
	mensage2 := <-canal

	fmt.Println(mensage)
	fmt.Println(mensage2)
}

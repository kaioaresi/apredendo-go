package main

import (
	"fmt"
	"time"
)

// Func escreve recebe dois parametros, sendo um mensage string e um canal tbm do tipo string
func escreve(mensage string, canal chan string) {
	for i := 0; i < 5; i++ {
		canal <- mensage // enviando um valor para canal
		time.Sleep(time.Second)
	}
	close(canal) // fechando o canal, uso indicado para evitar o deadlock
}

func main() {
	// criando um channel
	canal := make(chan string)

	fmt.Println("Inicio do programa.")

	go escreve("Teste K", canal)

	/* o canal tem duas operações que são eviar um dado e receber um dado, o problema aqui é elas são bloqueantes, quando esse canal
	receber a mensagem ele vai encerra a execução do problema
	*/

	// // Exemplo 1: Loop infinito, com uma condicional, caso o canal fechado encerra loop
	// for {
	// 	mensagem, aberto := <-canal // o canal está esperar receber um valor, caso o canal seja fechado ele encerra a execução
	// 	if !aberto {
	// 		break
	// 	}
	// 	fmt.Println(mensagem)

	// }

	// Exemplo 2: Loop simplificado, enguanto receber mensagem ele printa da tela, quando o canal fechar a execução é encerrada
	for mensagem := range canal {

		fmt.Println(mensagem)
	}

	fmt.Println("Finalizando programa!")

}

package main

import "fmt"

// func simples
func soma(n1 int, n2 int) int {
	return n1 + n2
}

// Nessa função teremos multiplos retornos
func calculos(n1, n2 int) (int, string) {
	soma := n1 + n2
	msg := "Calculos efetuados com sucesso!"
	return soma, msg
}

func main() {
	fmt.Println(soma(1, 2))

	// tipo func
	var f = func() {
		fmt.Println("Tipo Func!")
	}

	f()

	// tipo func com retorno
	var f2 = func(texto string) string {
		return texto
	}

	fmt.Println(f2("Teste K"))

	// Func mult retornos
	resultdoSoma, resultadoString := calculos(10, 50)
	fmt.Printf("O resultado da soma é %v\n%v\n", resultdoSoma, resultadoString)
}

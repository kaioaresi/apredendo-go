package main

import "fmt"

func main() {
	fmt.Println("##### Arrays básico #####")
	// array basico
	var array1 [5]int
	array1[0] = 11
	fmt.Println(array1)

	array2 := [3]string{"Lux", "Garen", "Isac"}
	fmt.Println(array2)
	fmt.Println()
	fmt.Println("##### Arrays semi-flexivel #####")
	// Array "semi-flexivel", ele preencher o valor fixo baseado na quantidade de elementos.
	array3 := [...]int{1, 2, 3, 4, 5}
	fmt.Println(array3)
	fmt.Println()
	// Slices
	fmt.Println("##### Slices #####")
	slice1 := []string{"Veigar", "Le Blanc", "Azir", "Brand"}
	slice1 = append(slice1, "Lilia")
	fmt.Println(slice1)
	fmt.Println()
	// Arrays internos
	fmt.Println("##### Arrays interno #####")
	slice3 := make([]float32, 10, 11)
	fmt.Println(slice3)
	// Fazendo um append para que o slice chegue ao seu limite
	slice3 = append(slice3, 12)
	fmt.Println(len(slice3)) // tamanho
	fmt.Println(cap(slice3)) // capacidade

	// Após chegarmos ao limite do slice, fazendo mais um append, vemos que o seu tamanho é dobrado
	slice3 = append(slice3, 10)
	fmt.Println(len(slice3)) // tamanho
	fmt.Println(cap(slice3)) // capacidade
}

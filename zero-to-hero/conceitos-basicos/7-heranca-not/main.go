package main

import "fmt"

type pessoa struct {
	nome      string
	sobrenome string
	idade     uint8
	altura    uint8
}

type estudante struct {
	pessoa    // vai herda todos campo da struct pessoa
	curso     string
	faculdade string
}

func main() {
	p1 := pessoa{"Blitz", "Crank", 99, 178}
	fmt.Println(p1)

	e1 := estudante{p1, "Mecanica", "Piltovar"}
	fmt.Println(e1.nome)
}

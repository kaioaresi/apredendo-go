package main

import "fmt"

func main() {
	// Maps
	usuario := map[string]string{
		"nome":      "Ziggs",
		"sobrenome": "Bom",
	}
	fmt.Println(usuario["nome"])
	fmt.Println(usuario["sobrenome"])

	delete(usuario, "nome")
	fmt.Println(usuario)

	usuario["nome"] = "Ziggs"
	fmt.Println(usuario)

	usuario2 := map[string]map[string]string{
		"endereco": {
			"rua":    "Rio negro",
			"cidade": "SP",
		},
	}

	usuario2["dados"] = map[string]string{
		"escola": "Nasa",
		"cor":    "Preto",
	}

	fmt.Println(usuario2)
}

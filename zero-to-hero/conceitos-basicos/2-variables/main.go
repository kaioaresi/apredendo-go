package main

import "fmt"

// func main() {
// 	var var1 string = "Darios" // declaração var + tipo + atribuição
// 	var var2 string            // declração + tipo
// 	var2 = "Garen"             // atribuição
// 	var3 := "Katarina"         // Inferência de tipo, quando o tipo é baseado no tipo de atribuição da variavel
// 	// Multiplas variaveis
// 	var (
// 		var4 string = "Teemo"
// 		var5 int    = 5
// 	)
// 	var6, var7 := "Vi", "Jinx" // Multiplas vars por inferência de tipo
// 	fmt.Println(var1)
// 	fmt.Println(var2)
// 	fmt.Println(var3)
// 	fmt.Println(var4, var5)
// 	fmt.Println(var6, var7)
// }

// func main() {
// 	const var1 string = "Lux" // declaração const + tipo + atribuição
// 	const var2 = 5
// 	const (
// 		var3 = "Draven"
// 		var4 = "TF"
// 	)

// 	const var5, var6 = "Sion", "Urgod"
// 	fmt.Println(var1)
// 	fmt.Println(var2)
// 	fmt.Println(var3, var4)
// 	fmt.Println(var5, var6)
// }

func main() {
	var1, var2 := "var 1", "var 2"
	fmt.Println(var1, var2) // valores originais

	var1, var2 = var2, var1 // alterando os valor
	fmt.Println(var1, var2)
}

package main

import (
	"errors"
	"fmt"
)

func main() {
	// Int
	var i int64 = 10000000
	fmt.Println(i)

	var i2 uint64 = 10000
	fmt.Println(i2)

	// Alias
	// int32 == Rune
	var r rune = 1231231
	fmt.Println(r)

	// Byte == uint8
	var b byte = 89
	fmt.Println(b)

	// Float
	var f float64 = 112.21
	fmt.Println(f)

	f2 := 112.212
	fmt.Println(f2)

	// Strings
	st := "Teste"
	fmt.Println(st)

	// boolean true/false
	var bl bool = true
	fmt.Println(bl)

	// error
	var err error = errors.New("Erro generico =)")
	fmt.Println(err)
}

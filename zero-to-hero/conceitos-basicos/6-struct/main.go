package main

import "fmt"

type endereco struct {
	logradouro string
	numero     int64
}

type usuario struct {
	nome     string
	idade    uint8
	endereco endereco
}

func main() {
	// Forma básica de declaração
	var user usuario
	user.nome = "Trindamer"
	user.idade = 100
	fmt.Printf("Nome: %v\nIdade: %v\n", user.nome, user.idade)

	// Forma resulmida
	endereco := endereco{"Rua X", 100}
	user2 := usuario{"Ashe", 30, endereco}
	fmt.Printf("Nome: %v\nIdade: %v\nna rua: %v número: %v\n", user2.nome, user2.idade, endereco.logradouro, endereco.numero)

	// Preenchendo apenas um campo
	user3 := usuario{nome: "Olaf"}
	fmt.Printf("Nome: %v\nIdade: %v\n", user3.nome, user3.idade)
}

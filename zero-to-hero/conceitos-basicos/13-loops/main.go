package main

import (
	"fmt"
	"time"
)

func main() {
	i := 0
	// Loop for simples
	for i < 10 {
		i++
		fmt.Println(i)
		time.Sleep(time.Second)
	}

	// For init
	for j := 0; j < 5; j++ {
		time.Sleep(time.Second)
		fmt.Println(j)
	}

	// For range
	nomes := [3]string{"Cassadim", "Katarina", "Nocturno"}

	// // Recebe um indice e um valor
	for _, value := range nomes {
		fmt.Println(value)
	}

	// Nesse caso, a letra será números da table ascii, vamos converter para string
	for _, letra := range "PALAVRA" {
		fmt.Println(string(letra))
	}

	map1 := map[string]string{
		"nome":       "Viktor",
		"habilidade": "evolução",
	}

	for chave, valor := range map1 {
		fmt.Println(chave, valor)
	}
}

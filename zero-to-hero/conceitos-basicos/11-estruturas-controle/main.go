package main

import "fmt"

func main() {
	n := 10
	// If else
	// If else básico
	if n > 10 {
		fmt.Println("Numero maior!")
	} else {
		fmt.Println("Numero igual ou menor!")
	}

	// if init
	if n2 := n; n2 > 0 {
		fmt.Println("Número maior que zero!")
	} else {
		fmt.Println("Número igual a zero!")
	}

	// if...else if...else
	if n3 := n; n3 > 0 {
		fmt.Println("Número maior que zero!")
	} else if n3 < 0 {
		fmt.Println("Número menor que zero!")
	} else {
		fmt.Println("Número igual a zero!")
	}

}

package main

import "fmt"

// Func q recebe uma interface generica que é atendida por qualquer coisa
func generica(interf interface{}) {
	fmt.Println(interf)
}

func main() {
	generica("Zyra")
	generica(true)
	generica(123)
}

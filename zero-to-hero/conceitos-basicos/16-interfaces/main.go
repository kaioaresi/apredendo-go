package main

import (
	"fmt"
	"math"
)

// Interface - receber uma assinatura de um metodo, que deve corresponder ao que foi declarado
type forma interface {
	area() float64
}

func escreverArea(f forma) {
	fmt.Printf("A àrea é %0.2f\n", f.area())
}

type retangulo struct {
	altura  float64
	largura float64
}

type circulo struct {
	raio float64
}

func (c circulo) area() float64 {
	return math.Pi * (c.raio * c.raio)
}

func (r retangulo) area() float64 {
	return r.altura * r.largura
}

func main() {
	r := retangulo{10, 20}
	c := circulo{3}
	escreverArea(r) // area do retangulo
	escreverArea(c) // area do circulo
}

package main

import "fmt"

func main() {

	// Aritimeticos
	soma := 2 + 2
	subtracao := 2 - 4
	divisao := 10 / 2
	multiplicacao := 20 * 3
	restoDivisao := 10 % 2
	fmt.Println(soma, subtracao, divisao, multiplicacao, restoDivisao)

	// Atribuição
	var var1 string = "Teemo"
	var2 := "Baron"
	fmt.Println(var1, var2)

	// Relationais
	fmt.Println(1 > 2)
	fmt.Println(1 < 2)
	fmt.Println(1 >= 2)
	fmt.Println(1 <= 2)
	fmt.Println(1 == 2)
	fmt.Println(1 != 2)

	// Lógicos
	fmt.Println("-------------")
	fmt.Println(true && true) // and
	fmt.Println(true || true) // or
	fmt.Println(!true)        // negação

	// Unários
	fmt.Println("-------------")
	n := 10
	n++
	fmt.Println(n)
	n--
	fmt.Println(n)
	n += 10
	fmt.Println(n)
	n *= 2
	fmt.Println(n)
	n -= 10
	fmt.Println(n)
	n /= 2
	fmt.Println(n)
	n %= 2
	fmt.Println(n)
}

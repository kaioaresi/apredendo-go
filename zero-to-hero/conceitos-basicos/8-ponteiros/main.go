package main

import "fmt"

func main() {
	fmt.Println("Ponteiros")
	var var1 int = 10
	var var2 int = var1
	fmt.Println(var1, var2)

	var1++                  // alterando o valor da var1 o valor da var2 é alterado ?
	fmt.Println(var1, var2) // não, pois var2 recebe uma copia de var1 apenas naquele momento, se alterarmos a var1, var2 não será afetada

	// Ponteiro é uma referencia de memoria
	var var3 int
	var ponteiro1 *int // O valor zero de um ponteiro é <nil>
	fmt.Println(var3, ponteiro1)

	var var5 int = 1000
	var ponteiro2 *int = &var5
	fmt.Println(var5, ponteiro2)
	fmt.Println("--------------")
	fmt.Println(var5, *ponteiro2) // agora vamor visualizar o valor que o ponteiro está apontando, esse processo é chamado de `desreferenciação`
}

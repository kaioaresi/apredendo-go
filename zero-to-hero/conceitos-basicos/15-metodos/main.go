package main

import "fmt"

type usuario struct {
	nome  string
	idade uint8
}

// Criando metodo
func (u usuario) salvar() {
	fmt.Printf("Metodos savar\nNome: %s - Idade: %v\n", u.nome, u.idade)
}

// Treinando - trocando nome de todos usuário para "Le Blank"
func (u *usuario) trocaNome() {
	u.nome = "Le Blank"
}

func main() {
	user1 := usuario{"Garen", 25}
	fmt.Println(user1)
	user1.salvar() // chamando o metodo salvar
	user1.trocaNome()
	fmt.Println(user1)
}

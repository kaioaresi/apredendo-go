package main

import "fmt"

// Switch básico 1
func diaDaSemana(n int) string {
	switch n {
	case 1:
		return "Domingo"
	case 2:
		return "Segunda-feira"
	case 3:
		return "Terça-feora"
	case 4:
		return "Quarta-feira"
	case 5:
		return "Quinta-feira"
	case 6:
		return "Sexta-feira"
	case 7:
		return "Sábado"
	default:
		return "Número invalido!"
	}
}

// Switch básico 2
func diaDaSemana2(n int) string {
	switch {
	case n == 1:
		return "Domingo"
	case n == 2:
		return "Segunda-feira"
	case n == 3:
		return "Terça-feora"
	case n == 4:
		return "Quarta-feira"
	case n == 5:
		return "Quinta-feira"
	case n == 6:
		return "Sexta-feira"
	case n == 7:
		return "Sábado"
	default:
		return "Número invalido!"
	}
}

// Switch básico 3
func diaDaSemana3(n int) string {
	var diaDaSemana string

	switch n {
	case 1:
		diaDaSemana = "Domingo"
	case 2:
		diaDaSemana = "Segunda-feira"
	case 3:
		diaDaSemana = "Terça-feora"
	case 4:
		diaDaSemana = "Quarta-feira"
	case 5:
		diaDaSemana = "Quinta-feira"
	case 6:
		diaDaSemana = "Sexta-feira"
	case 7:
		diaDaSemana = "Sábado"
	default:
		diaDaSemana = "Número invalido!"
	}

	return diaDaSemana
}

func main() {
	fmt.Println(diaDaSemana(3))
	fmt.Println(diaDaSemana2(7))
	fmt.Println(diaDaSemana3(0))
}

package main

import "fmt"

func test1() {
	fmt.Println("Mensagem generica 1")
}

func test2() {
	fmt.Println("Mensagem generica 2")
}

func alunoAprovadoReprovado(n1, n2 float32) bool {
	defer fmt.Println("Média foi calculada!!!")
	fmt.Println("Verificando se aluno foi aprovado:...")
	media := (n1 + n2) / 2
	if media >= 6 {
		return true
	}
	return false
}

func main() {
	// Uso do defer em func
	// defer test1() // defer == adiar - adia a executação da func até o ultimo momento possivel
	// test2()

	fmt.Println(alunoAprovadoReprovado(9, 7))
}

package main

import "fmt"

// Função recursiva
func fibonati(posicao uint) uint {
	if posicao <= 1 {
		return posicao
	}

	return fibonati(posicao-2) + fibonati(posicao-1)
}

func main() {
	posicao := uint(15)
	// fmt.Println(fibonati(posicao))

	for i := uint(0); i < posicao; i++ {
		fmt.Println(fibonati(i))
	}

}

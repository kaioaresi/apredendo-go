package main

import "fmt"

// Func variatica - pode receber n parametros
func soma(n ...int) int {
	total := 0

	for _, value := range n {
		total += value
	}
	return total
}

// Só é possive apenas um parametro variatico `...` em uma função e ele deve ser sempre o ultimo
func msgTest(nome string, numero ...int) {
	for _, v := range numero {
		fmt.Println(nome, v)
	}
}

func main() {
	fmt.Println(soma(1, 2, 4, 5, 6))
	msgTest("Teemo", 1, 2, 3, 4, 5, 6)
}

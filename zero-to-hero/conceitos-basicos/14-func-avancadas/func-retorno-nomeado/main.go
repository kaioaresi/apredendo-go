package main

import "fmt"

// Return nomeado
func calculoMatematico(n1, n2 int) (soma int, subtracao int) {
	soma = n1 + n2
	subtracao = n1 - n2
	return // como já informamos que o retorno será soma e subtração não é necessário informa-los no return
}

func main() {
	fmt.Println(calculoMatematico(5, 10))
}

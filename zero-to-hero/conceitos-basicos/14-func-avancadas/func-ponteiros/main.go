package main

import "fmt"

func inverterSinal(n int) int {
	return n * -1
}

// Recebe um parametro de endereço de memoria
func inverterNumeroPonteiro(n *int) {
	*n = *n * -1 // alterar o conteudo do endereço de memoria
}

func main() {
	n1 := 10
	fmt.Println(inverterSinal(10))
	inverterNumeroPonteiro(&n1) // alterando o valor via endereçamento de memoria
	fmt.Println(n1)
}

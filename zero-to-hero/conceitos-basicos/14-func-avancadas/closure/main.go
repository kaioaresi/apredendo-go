package main

import "fmt"

// func são como tipos em GO sendo assim pode retorna-las ou passar como parametro

func closure() func() {
	texto := "Mensagem closure"
	msgFunc := func() {
		fmt.Println(texto)
	}

	return msgFunc
}

func main() {
	texto := "Main"
	fmt.Println(texto)

	funcNova := closure()

	funcNova()
}

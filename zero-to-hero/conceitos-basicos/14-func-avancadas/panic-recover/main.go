package main

import "fmt"

func recoveyExecucao() {
	// A func recover tenta recuperar a executação
	if r := recover(); r != nil {
		fmt.Println("Execução repuerada com sucesso!")
	}
}

func mediaNota(n1, n2 float32) bool {
	defer recoveyExecucao() // será executando antes do panic
	media := (n1 + n2) / 2

	if media > 6 {
		return true
	} else if media < 6 {
		return false
	}

	panic("Média igual a 6!") // entra em pane
}

func main() {
	fmt.Println(mediaNota(6, 8))
	fmt.Println("Mensagem pós execução!") // a func mediaNota entre em panic essa linha não será executada
}

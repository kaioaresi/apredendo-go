package main

import "fmt"

func main() {
	// funçã anônima - não possui nome e para executa-la basta utilizar o `()` após a ultima chaves da func
	func() {
		fmt.Println("Olá, nundo!")
	}() // <<< para executar a func anônina

	msgTexto := "Olá, func anônia"
	// Passando parametros
	func(texto string) {
		fmt.Println(texto)
	}(msgTexto)

	// Return
	msgRetorno := func(texto string) string {
		return fmt.Sprintf("Mensagem recebida >>> %s\n", texto)
	}(msgTexto)

	fmt.Println(msgRetorno)

	// Test - é possível utilizar a func anonima com func variatica
	func(msg string, numeros ...int) {
		for _, valor := range numeros {
			fmt.Println(msg, valor)
		}
	}("Darios", 1, 2, 3, 4, 5, 10, 20, 30)
}

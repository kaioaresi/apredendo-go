package main

import "fmt"

// É uma func que é executada antes da main, cada arquivo pode ter essa func, é bem utilizada para inicialização de var e etc
func init() {
	fmt.Println("Func init!")
}

func main() {
	fmt.Println("Func main")
}

package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql" // import implicito
)

func main() {
	// "usuario:senha@banco"
	stringConexao := "golang:golang@/devbook?parseTime=True&loc=Local"
	db, err := sql.Open("mysql", stringConexao)

	if err != nil {
		log.Fatal(err)
	}

	defer db.Close() // Encerra a conexão antes de finalizar a func

	// Check ping
	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("Conexão com banco ok!")

	// Executando query no banco
	linhas, err := db.Query("select * from usuarios;")

	defer linhas.Close() // Fecha as linhas

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(linhas)
}

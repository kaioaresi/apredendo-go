package banco

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql" // import implicito drive de conexão com mysql
)

// Func conexaoDB - conectar a base de dados
func ConexaoDB() (*sql.DB, error) {
	stringConexao := "golang:golang@/devbook?parseTime=True&loc=Local"

	db, err := sql.Open("mysql", stringConexao)

	if err != nil {
		return nil, err
	}

	// Verificando conexão
	if err = db.Ping(); err != nil {
		return nil, err
	}

	fmt.Println("Conectado ao banco de dados!...")
	return db, nil
}

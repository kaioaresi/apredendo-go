package servidor

import (
	"crud-db/banco"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type usuario struct {
	ID    uint32 `json:"id"`
	Nome  string `json:"nome"`
	Email string `json:"email"`
}

// Func CriarUsuarios - insere usuário no banco de dados
func CriarUsuarios(w http.ResponseWriter, r *http.Request) {
	bodyRequest, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.Write([]byte("Falha ao receber o corpo da requet!"))
		return // Como é possível continucar sem o body esse return vário encerra a execução
	}

	var usuario usuario

	if err = json.Unmarshal(bodyRequest, &usuario); err != nil {
		w.Write([]byte("Erro ao coverter usuário para struct."))
		return // Como é possível continucar sem o body esse return vário encerra a execução
	}

	db, err := banco.ConexaoDB()
	if err != nil {
		w.Write([]byte("Erro ao conectar no banco!"))
		return
	}

	// Para evitar o sqlinjection
	statment, err := db.Prepare("insert into usuarios (nome, email) values (?,?)")
	if err != nil {
		w.Write([]byte("Erro ao criar o statment"))
		return
	}
	defer statment.Close()

	insertTable, err := statment.Exec(usuario.Nome, usuario.Email)
	if err != nil {
		w.Write([]byte("Erro ao executar o statment!"))
		return
	}

	idInserted, err := insertTable.LastInsertId()
	if err != nil {
		w.Write([]byte("Erro ao realizar o get do id do usuário"))
		return
	}

	// Status code esperado 201
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf("Usuário id: %d inserido com sucesso!", idInserted)))

}

// Func BuscarUsuarios - pesquisa usuários
func BuscarUsuarios(w http.ResponseWriter, r *http.Request) {
	db, err := banco.ConexaoDB()
	if err != nil {
		w.Write([]byte("Erro ao conectar com banco de dados!"))
	}

	defer db.Close()

	linhas, err := db.Query("SELECT * FROM usuarios")
	if err != nil {
		w.Write([]byte("Erro ao buscar usuários!"))
		return
	}

	defer linhas.Close()

	var sliceUsuarios []usuario
	for linhas.Next() {
		var usuario usuario
		if err := linhas.Scan(&usuario.ID, &usuario.Nome, &usuario.Email); err != nil {
			w.Write([]byte("Erro ao scan usuário!"))
			return
		}

		sliceUsuarios = append(sliceUsuarios, usuario)

	}

	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(sliceUsuarios); err != nil {
		w.Write([]byte("Erro ao converter em json!"))
		return
	}

}

// Func BuscarUsuario - pesquisa um usuários especifico
func BuscarUsuario(w http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)

	ID, err := strconv.ParseUint(parametros["id"], 10, 32)
	if err != nil {
		w.Write([]byte("Erro ao converter parametros para int."))
		return
	}

	db, err := banco.ConexaoDB()
	if err != nil {
		w.Write([]byte("Erro ao buscar usuário!"))
		return
	}

	linha, err := db.Query("SELECT * from usuarios where id = ?", ID)
	if err != nil {
		w.Write([]byte("Erro ao realizar consulta do usuário"))
		return
	}

	var usuario usuario

	if linha.Next() {
		if err := linha.Scan(&usuario.ID, &usuario.Nome, &usuario.Email); err != nil {
			w.Write([]byte("Erro ao scannear usuário!"))
			return
		}
	}

	if err := json.NewEncoder(w).Encode(usuario); err != nil {
		w.Write([]byte("Erro ao encoder usuário."))
		return
	}

}

// Func AtualizarUsuario - faz update de informações do usuário no banco
func AtualizarUsuario(w http.ResponseWriter, r *http.Request) {
	parametro := mux.Vars(r)

	ID, err := strconv.ParseUint(parametro["id"], 10, 32)
	if err != nil {
		w.Write([]byte("Erro ao converter parametro para inteiro!"))
		return
	}

	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.Write([]byte("Erro ao ler o corpo da request"))
		return
	}

	var usuario usuario

	if err := json.Unmarshal(requestBody, &usuario); err != nil {
		w.Write([]byte("Erro ao converter para json."))
		return
	}

	db, err := banco.ConexaoDB()
	if err != nil {
		w.Write([]byte("Erro na conexão com banco de dados."))
	}

	defer db.Close()

	statment, err := db.Prepare("UPDATE usuarios SET nome = ?, email = ? WHERE id = ?")
	if err != nil {
		w.Write([]byte("Erro ao criar o statment."))
		return
	}

	defer statment.Close()

	if _, err := statment.Exec(usuario.Nome, usuario.Email, ID); err != nil {
		w.Write([]byte("Erro ao atualizar o usuário."))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Func DeleteUsuario - deleta um usuário
func DeleteUsuario(w http.ResponseWriter, r *http.Request) {
	parametro := mux.Vars(r)

	ID, err := strconv.ParseUint(parametro["id"], 10, 32)
	if err != nil {
		w.Write([]byte("Erro ao converter parametro!"))
		return
	}

	db, err := banco.ConexaoDB()
	if err != nil {
		w.Write([]byte("Erro na conexão com banco de dados!"))
	}

	defer db.Close()

	statment, err := db.Prepare("DELETE FROM usuarios where id = ?")
	if err != nil {
		w.Write([]byte("Erro ao criar statment."))
		return
	}

	defer statment.Close()

	if _, err := statment.Exec(ID); err != nil {
		w.Write([]byte("Erro ao deletar o usuário"))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

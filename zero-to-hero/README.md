# Golang - Zero To hero

> Minha jornada golang

```mermaid
journey
    title Minha jornada Golang
    section Aula 1
      Func main: "Hello, world!" 9: kaio
      Module: Criando um module 8: kaio
      Pacotes externos: Utilizando pacotes externos 9: kaio
```

- Extremamente eficiente
    - Compilada
    - Otimizada para usar mais de um núcleo do processador
    - Feita para lidar muito bem com concorrêcia

- Simplicidade + Robustez
    - Simplicidade de uma linguagem como python ou javascript
    - Robustez de uma linguagem como C# ou java
- Criada pelo Google
    - A linguagem foi criada para resolver problemas da gigante da tecnologia
    - Continua sendo mantida pela empresa
    - Diversos serviços do Google que você utiliza todos os dias são escritos em `Go`!

## Estrutura do curso

- Fundamentos da linguagem
- Fundamentos adicionais
- Desenvolvimento de uma aplicação completa

## Criando um modulo

Passo 1:

Crie uma pasta para o projeto

```bash
mkdir <projeto>
```

Passo 2:

Crie uma estrutura de module

```bash
go mod init <nome moduloe> # execute dentro do path desejado
go build # execute dentro do path desejado e será criado um arquivo binario com nome do modulo 
```

Passo 3:

Após sucesso nos passos anteriores, crie uma nova pasta dentro da pasta anterior, seguimentando o código exemplo `pacotes/{auxiliar}`

Em uma arquivo auxiliar escreve seu código

```golang
package auxiliar

import "fmt"

// Escrever(): exibe uma mensagem na tela
func Escrever() {
	fmt.Println("Escrevendo do arquivo auxiliar!")
}
```

> **Peculiaridade do golang:** se a função dentro do path auxiliar inicia com letra maiuscula significa que ela é uma func publica, se minuscula significa que ela é uma func local, ou seja acessivel apenas dentro do proprio pacote

> **Boa pratica:** é uma boa pratica escrever um comentario acima da função, explicando o que ela faz.

Passo 4:

No path main crie um arquivo `main.go`, e invoque a func auxiliar

```golang
package main

import (
	"pacotes/auxiliar"
)

func main() {
	auxiliar.Escrever()
}
```

## Pacotes externos

No raiz do projeto execute:

> Será configurado em seu arquivo `go.mod` essa nova dependencia

```bash
go get github.com/badoux/checkmail
```

Agora vamos utilizar esse pacote

```golang
import (
	"github.com/badoux/checkmail"
)

func main() {
	fmt.Println(checkmail.ValidateFormat("meuemail@gmail.com"))
}
```

## Variables

Go é uma linguagem fortemente tipada, ou seja é ideial você conhecer bem os tipos disponiveis para utilização, existem algumas formas de se declarar uma variavel:

```golang
func main() {
	var var1 string = "Darios" // declaração var + tipo + atribuição
	var var2 string            // declração + tipo
	var2 = "Garen"             // atribuição
	var3 := "Katarina"         // Inferência de tipo, quando o tipo é baseado no tipo de atribuição da variavel
	// Multiplas variaveis
	var (
		var4 string = "Teemo"
		var5 int    = 5
	)
	fmt.Println(var1)
	fmt.Println(var2)
	fmt.Println(var3)
	fmt.Println(var4)
	fmt.Println(var5)
}
```

> Tricks com variables: imagine que você precisa trocar os valor entre duas variavel, em go isso pode ser feito de uma maneira muito simples

```golang
func main() {
	var1, var2 := "var 1", "var 2"
	fmt.Println(var1, var2) // valores originais

	var1, var2 = var2, var1 // alterando os valor
	fmt.Println(var1, var2)
}
```

## Constantes

A deplaração de constantes é muito proxima a declaração de uma variavel comum.


```golang
func main() {
	const var1 string = "Lux" // declaração const + tipo + atribuição
	const var2 = 5
	const (
		var3 = "Draven"
		var4 = "TF"
	)

	const var5, var6 = "Sion", "Urgod"
	fmt.Println(var1)
	fmt.Println(var2)
	fmt.Println(var3, var4)
	fmt.Println(var5, var6)
}

```

## Tipos de dados

> **Lembrete:** Toda var criada sem um valor atribuido possui um valor default chamado de `valor zero`.


```golang
func main() {
	// Int
	var i int64 = 10000000
	fmt.Println(i)

	var i2 uint64 = 10000
	fmt.Println(i2)

	// Alias
	// int32 == Rune
	var r rune = 1231231
	fmt.Println(r)

	// Byte == uint8
	var b byte = 89
	fmt.Println(b)

	// Float
	var f float64 = 112.21
	fmt.Println(f)

	f2 := 112.212
	fmt.Println(f2)

	// Strings
	st := "Teste"
	fmt.Println(st)

	// boolean true/false
	var bl bool = true
	fmt.Println(bl)

	// error
	var err error = errors.New("Erro generico =)")
	fmt.Println(err)
}
```

Existe além dos tipos básico o tipo `func()`

```golang
func main() {
	// tipo func
	var f = func() {
		fmt.Println("Tipo Func!")
	}

	f()
}
```

Exemplo com returno

```golang
func main() {
	// tipo func com retorno
	var f2 = func(texto string) string {
		return texto
	}

	fmt.Println(f2("Teste K"))
}
```

## Func básica

Função com apenas um tipo de retorno

```golang
// func simples
func soma(n1 int, n2 int) int {
	return n1 + n2
}

func main() {
	fmt.Println(soma(1, 2))
}
```

Multiplos retornos

```golang
// Nessa função teremos multiplos retornos
func calculos(n1, n2 int) (int, string) {
	soma := n1 + n2
	msg := "Calculos efetuados com sucesso!"
	return soma, msg
}

func main() {
	// Func mult retornos
	resultdoSoma, resultadoString := calculos(10, 50)
	fmt.Printf("O resultado da soma é %v\n%v\n", resultdoSoma, resultadoString)
}
```

## Operadores


```golang
func main() {

	// Operadores Aritimeticos
	soma := 2 + 2
	subtracao := 2 - 4
	divisao := 10 / 2
	multiplicacao := 20 * 3
	restoDivisao := 10 % 2
	fmt.Println(soma, subtracao, divisao, multiplicacao, restoDivisao)

	// Operadores Atribuição
	var var1 string = "Teemo"
	var2 := "Baron"
	fmt.Println(var1, var2)

	// Operadores Relationais
	fmt.Println(1 > 2)
	fmt.Println(1 < 2)
	fmt.Println(1 >= 2)
	fmt.Println(1 <= 2)
	fmt.Println(1 == 2)
	fmt.Println(1 != 2)

	// Operadores Lógicos
	fmt.Println("-------------")
	fmt.Println(true && true) // and
	fmt.Println(true || true) // or
	fmt.Println(!true)        // negação

	// Operadores Unários
	fmt.Println("-------------")
	n := 10
	n++
	fmt.Println(n)
	n--
	fmt.Println(n)
	n += 10
	fmt.Println(n)
	n *= 2
	fmt.Println(n)
	n -= 10
	fmt.Println(n)
	n /= 2
	fmt.Println(n)
	n %= 2
	fmt.Println(n)
}
```

# Struct

```golang
type usuario struct {
	nome  string
	idade int
}

func main() {
	var user usuario
	user.nome = "Trindamer"
	user.idade = 1000
	fmt.Printf("Nome: %v\nIdade: %v\n", user.nome, user.idade)
	user2 := usuario{"Ashe", 30}
	fmt.Printf("Nome: %v\nIdade: %v\n", user2.nome, user2.idade)
}
```

## Algo parecido com herança

```golang
type pessoa struct {
	nome      string
	sobrenome string
	idade     uint8
	altura    uint8
}

type estudante struct {
	pessoa    // vai herda todos campo da struct pessoa
	curso     string
	faculdade string
}

func main() {
	p1 := pessoa{"Blitz", "Crank", 99, 178}
	fmt.Println(p1)

	e1 := estudante{p1, "Mecanica", "Piltovar"}
	fmt.Println(e1.nome)
}
```

## Ponteiros

```golang
func main() {
	fmt.Println("Ponteiros")
	var var1 int = 10
	var var2 int = var1
	fmt.Println(var1, var2)

	var1++                  // alterando o valor da var1 o valor da var2 é alterado ?
	fmt.Println(var1, var2) // não, pois var2 recebe uma copia de var1 apenas naquele momento, se alterarmos a var1, var2 não será afetada

	// Ponteiro é uma referencia de memoria
	var var3 int
	var ponteiro1 *int // O valor zero de um ponteiro é <nil>
	fmt.Println(var3, ponteiro1)

	var var5 int = 1000
	var ponteiro2 *int = &var5
	fmt.Println(var5, ponteiro2)
	fmt.Println("--------------")
	fmt.Println(var5, *ponteiro2) // agora vamor visualizar o valor que o ponteiro está apontando, esse processo é chamado de `desreferenciação`
}
```

## Arrays e Slices

```golang
func main() {
	fmt.Println("##### Arrays básico #####")
	// array basico
	var array1 [5]int
	array1[0] = 11
	fmt.Println(array1)

	array2 := [3]string{"Lux", "Garen", "Isac"}
	fmt.Println(array2)

	fmt.Println("##### Arrays semi-flexivel #####")
	// Array "semi-flexivel", ele preencher o valor fixo baseado na quantidade de elementos.
	array3 := [...]int{1, 2, 3, 4, 5}
	fmt.Println(array3)

	// Slices
	fmt.Println("##### Slices #####")
	slice1 := []string{"Veigar", "Le Blanc", "Azir", "Brand"}
	slice1 = append(slice1, "Lilia")
	fmt.Println(slice1)

	// Arrays internos
	fmt.Println("##### Arrays interno #####")
	slice3 := make([]float32, 10, 11)
	fmt.Println(slice3)
	// Fazendo um append para que o slice chegue ao seu limite
	slice3 = append(slice3, 12)
	fmt.Println(len(slice3)) // tamanho
	fmt.Println(cap(slice3)) // capacidade

	// Após chegarmos ao limite do slice, fazendo mais um append, vemos que o seu tamanho é dobrado
	slice3 = append(slice3, 10)
	fmt.Println(len(slice3)) // tamanho
	fmt.Println(cap(slice3)) // capacidade
}
```

> **Resumo:** arrays é uma lista com tamanho fixo, já o slice é uma lista sem tamanho fixo.

## Maps

```golang
func main() {
	// Maps
	usuario := map[string]string{
		"nome":      "Ziggs",
		"sobrenome": "Bom",
	}
	fmt.Println(usuario["nome"])
	fmt.Println(usuario["sobrenome"])

	delete(usuario, "nome")
	fmt.Println(usuario)

	usuario["nome"] = "Ziggs"
	fmt.Println(usuario)

	usuario2 := map[string]map[string]string{
		"endereco": {
			"rua":    "Rio negro",
			"cidade": "SP",
		},
	}

	usuario2["dados"] = map[string]string{
		"escola": "Nasa",
		"cor":    "Preto",
	}

	fmt.Println(usuario2)
}
```
> Resumo: Todas chaves devem ser do mesmo tipo `map[string]` e todos valores também deve ser do mesmo tipo `map[string]int`

## Estruturas de cotrole

```golang
func main() {
	n := 10

	// If else básico
	if n > 10 {
		fmt.Println("Numero maior!")
	} else {
		fmt.Println("Numero igual ou menor!")
	}

	// if init
	if n2 := n; n2 > 0 {
		fmt.Println("Número maior que zero!")
	} else {
		fmt.Println("Número igual a zero!")
	}

	// if...else if...else
	if n3 := n; n3 > 0 {
		fmt.Println("Número maior que zero!")
	} else if n3 < 0 {
		fmt.Println("Número menor que zero!")
	} else {
		fmt.Println("Número igual a zero!")
	}
}
```

## Switch

Exemplo básico 1
```golang
// Switch básico 1
func diaDaSemana(n int) string {
	switch n {
	case 1:
		return "Domingo"
	case 2:
		return "Segunda-feira"
	case 3:
		return "Terça-feora"
	case 4:
		return "Quarta-feira"
	case 5:
		return "Quinta-feira"
	case 6:
		return "Sexta-feira"
	case 7:
		return "Sábado"
	default:
		return "Número invalido!"
	}
}
```

Exemplo básico 2
```golang
// Switch básico 2
func diaDaSemana2(n int) string {
	switch {
	case n == 1:
		return "Domingo"
	case n == 2:
		return "Segunda-feira"
	case n == 3:
		return "Terça-feora"
	case n == 4:
		return "Quarta-feira"
	case n == 5:
		return "Quinta-feira"
	case n == 6:
		return "Sexta-feira"
	case n == 7:
		return "Sábado"
	default:
		return "Número invalido!"
	}
}
```

Exemplo básico 3
```golang
// Switch básico 3
func diaDaSemana3(n int) string {
	var diaDaSemana string

	switch n {
	case 1:
		diaDaSemana = "Domingo"
	case 2:
		diaDaSemana = "Segunda-feira"
	case 3:
		diaDaSemana = "Terça-feora"
	case 4:
		diaDaSemana = "Quarta-feira"
	case 5:
		diaDaSemana = "Quinta-feira"
	case 6:
		diaDaSemana = "Sexta-feira"
	case 7:
		diaDaSemana = "Sábado"
	default:
		diaDaSemana = "Número invalido!"
	}

	return diaDaSemana
}
```

## Loops

```golang
func main() {
	i := 0
	// Loop for simples
	for i < 10 {
		i++
		fmt.Println(i)
		time.Sleep(time.Second)
	}

	For init
	for j := 0; j < 5; j++ {
		time.Sleep(time.Second)
		fmt.Println(j)
	}

	// For range
	nomes := [3]string{"Cassadim", "Katarina", "Nocturno"}

	// // Recebe um indice e um valor
	for _, value := range nomes {
		fmt.Println(value)
	}

	// Nesse caso, a letra será números da table ascii, vamos converter para string
	for _, letra := range "PALAVRA" {
		fmt.Println(string(letra))
	}

	map1 := map[string]string{
		"nome":       "Viktor",
		"habilidade": "evolução",
	}

	for chave, valor := range map1 {
		fmt.Println(chave, valor)
	}
}
```

> Aviso: Não é possível utilizar o `for` + `range` para `struct`

## Funções avançadas

### Func retorno nomeado

```golang
// Return nomeado
func calculoMatematico(n1, n2 int) (soma int, subtracao int) {
	soma = n1 + n2
	subtracao = n1 - n2
	return // como já informamos que o retorno será soma e subtração não é necessário informa-los no return
}

func main() {
	fmt.Println(calculoMatematico(5, 10))
}
```

### Função variática

```golang
// Func variatica - pode receber n parametros
func soma(n ...int) int {
	total := 0

	for _, value := range n {
		total += value
	}
	return total
}

// Só é possive utilizar apenas um parametro variatico `...` em uma função e ele deve ser sempre o ultimo
func msgTest(nome string, numero ...int) {
	for _, v := range numero {
		fmt.Println(nome, v)
	}
}

func main() {
	fmt.Println(soma(1, 2, 4, 5, 6))
	msgTest("Teemo", 1, 2, 3, 4, 5, 6)
}
```

### Função anônima

```golang
func main() {
	// funçã anônima - não possui nome e para executa-la basta utilizar o `()` após a ultima chaves da func
	func() {
		fmt.Println("Olá, nundo!")
	}() // <<< para executar a func anônina

	msgTexto := "Olá, func anônia"
	// Passando parametros
	func(texto string) {
		fmt.Println(texto)
	}(msgTexto)

	// Return
	msgRetorno := func(texto string) string {
		return fmt.Sprintf("Mensagem recebida >>> %s\n", texto)
	}(msgTexto)

	fmt.Println(msgRetorno)

	// Test - é possível utilizar a func anonima com func variatica
	func(msg string, numeros ...int) {
		for _, valor := range numeros {
			fmt.Println(msg, valor)
		}
	}("Darios", 1, 2, 3, 4, 5, 10, 20, 30)
}
```


### Funções recursivas

```golang
// Função recursiva
func fibonati(posicao uint) uint {
	if posicao <= 1 {
		return posicao
	}

	return fibonati(posicao-2) + fibonati(posicao-1)
}

func main() {
	posicao := uint(15)
	fmt.Println(fibonati(posicao))

	for i := uint(0); i < posicao; i++ {
		fmt.Println(fibonati(i))
	}

}
```

### Defer

```golang
func test1() {
	fmt.Println("Mensagem generica 1")
}

func test2() {
	fmt.Println("Mensagem generica 2")
}

func alunoAprovadoReprovado(n1, n2 float32) bool {
	defer fmt.Println("Média foi calculada!!!")
	fmt.Println("Verificando se aluno foi aprovado:...")
	media := (n1 + n2) / 2
	if media >= 6 {
		return true
	}
	return false
}

func main() {
	// Uso do defer em func
	defer test1() // defer == adiar - adia a executação da func até o ultimo momento possivel
	test2()

	fmt.Println(alunoAprovadoReprovado(9, 7))
}
```

### Panic e Recover

```golang
func recoveyExecucao() {
	// A func recover tenta recuperar a executação
	if r := recover(); r != nil {
		fmt.Println("Execução repuerada com sucesso!")
	}
}

func mediaNota(n1, n2 float32) bool {
	defer recoveyExecucao() // será executando antes do panic
	media := (n1 + n2) / 2

	if media > 6 {
		return true
	} else if media < 6 {
		return false
	}

	panic("Média igual a 6!") // entra em pane
}

func main() {
	fmt.Println(mediaNota(6, 8))
	fmt.Println("Mensagem pós execução!") // a func mediaNota entre em panic essa linha não será executada
}
```

### Closure

```golang

// func são como tipos em GO sendo assim pode retorna-las ou passar como parametro
func closure() func() {
	texto := "Mensagem closure"
	msgFunc := func() {
		fmt.Println(texto)
	}

	return msgFunc
}

func main() {
	texto := "Main"
	fmt.Println(texto)

	funcNova := closure()

	funcNova() // Será exibido a o texto dendo da func closure e n o dentro da func main
}
```

### Func ponteiros

```golang
// Apenas criar uma copia e exibe o valor recebido
func inverterSinal(n int) int {
	return n * -1
}

// Recebe um parametro de endereço de memoria o altera diretamente na memoria, nesse caso não é necessário um retorno, pois a alteração é diretamento no endereço do parametro
func inverterNumeroPonteiro(n *int) {
	*n = *n * -1 // alterar o conteudo do endereço de memoria
}

func main() {
	n1 := 10
	fmt.Println(inverterSinal(10))
	inverterNumeroPonteiro(&n1) // alterando o valor via endereçamento de memoria
	fmt.Println(n1)
}
```

### Init

```golang
// É uma func que é executada antes da main, cada arquivo pode ter essa func, é bem utilizada para inicialização de var e etc
func init() {
	fmt.Println("Func init!")
}

func main() {
	fmt.Println("Func main")
}
```

## Metodos

```golang
type usuario struct {
	nome  string
	idade uint8
}

// Criando metodo
func (u usuario) salvar() {
	fmt.Printf("Metodos savar\nNome: %s - Idade: %v\n", u.nome, u.idade)
}

// Treinando - trocando nome de todos usuário para "Le Blank"
func (u *usuario) trocaNome() {
	u.nome = "Le Blank"
}

func main() {
	user1 := usuario{"Garen", 25}
	fmt.Println(user1)
	user1.salvar() // chamando o metodo salvar
	user1.trocaNome()
	fmt.Println(user1)
}
```

## Interfaces

```golang

type retangulo struct {
	altura  float64
	largura float64
}

type circulo struct {
	raio float64
}

// Interface - receber uma assinatura de um metodo, que deve corresponder ao que foi declarado
type forma interface {
	area() float64
}

func (c circulo) area() float64 {
	return math.Pi * (c.raio * c.raio)
}

func (r retangulo) area() float64 {
	return r.altura * r.largura
}

func escreverArea(f forma) {
	fmt.Printf("A àrea é %0.2f\n", f.area())
}

func main() {
	r := retangulo{10, 20}
	c := circulo{3}
	escreverArea(r) // area do retangulo
	escreverArea(c) // area do circulo
}
```

### Interface generica 

> Alerta: cuidado para que esse tipo de utilização não vire uma gambiara =)

```golang
// Func q recebe uma interface generica que é atendida por qualquer coisa
func generica(interf interface{}) {
	fmt.Println(interf)
}

func main() {
	generica("Zyra")
	generica(true)
	generica(123)
}
```

## Mátematica em go

Operação | O que ela retorna
:---:|:---:
x + y | Soma de x e y
x - y | Diferença entre x e y
-x | Muda o sinal de x
+x | Identidade de x
x * y | Produto de x e y
x / y | Quociente de x e y
x % y | Resto de x / y


Ordem | Letra | Representa
:---:|:---:|:---:
1 | P | Parênteses
2 | E | Expoente
3 | M | Multiplicação
4 | D | Divisão
5 | A | Adição
6 | S | Subtração


Finalizando conceitos básicos

---

## Concorência


### Goroutine

Pense em uma goroutine como uma thread, conceito que você provavelmente já está familiarizado com outras linguagens de programação. Em Go, você pode gerar uma nova goroutine para executar código simultaneamente usando a keyword go:

```golang
func escreve(t string) {
	for {
		fmt.Println(t)
		time.Sleep(time.Second)
	}
}

// Concorrência != paralelismo
func main() {
	go escreve("Teste 123") // gorountine - começe a execução mas n espere termina e já siga o script
	escreve("Teste k")      // execução normal
}
```

### Waitgroup

WaitGroup é na verdade um tipo de contador que bloqueia a execução da função (ou pode dizer A goroutine) até que seu contador interno se torne 0 

```golang
func escreve(t string) {
	for i := 0; i < 5; i++ {
		fmt.Println(t)
		time.Sleep(time.Second)
	}
}

func main() {
	var waitGroup sync.WaitGroup

	waitGroup.Add(2) // quantidade de goroutines que ele precisa esperar terminar, antes de finalizar o programa

	// goroutine 1
	go func() {
		escreve("Teste 123")
		waitGroup.Done() // avisa que uma goroutine foi finalizada, remove uma goroutine do contado
	}()
	// goroutine 2
	go func() {
		escreve("Teste k")
		waitGroup.Done()
	}()

	waitGroup.Wait() // informa para func main, para ele esperar ambas goroutine finalizar
}
```

### Channels

Channels são como "tubos" que conectam goroutines simultâneas. Você pode enviar valores para channels de uma goroutine e receber esses valores em outra goroutine.

![Channels](../img/channels1.svg)

Com channels você consegue fazer com que duas goroutines convercem entre sí, ou até que uma goroutine espere mensagens dentro de um channel no qual várias goroutines podem alterá-lo

![Channels 2](../img/channels2.webp)


#### Sintaxe básica

```golang
func main() {
	// Criando um canal do tipo string com buffer de 1 mensage apenas
	menssage := make(chan string, 1)

	// Enviando uma mensagem para o canal
	menssage <- "Texto qualquer"

	// Recebendo a mensage no canal
	msg := <-menssage

	fmt.Println(msg)
}
```


#### Channels directions

Ao usar channels como parâmetro de função, você pode especificar se um channel destina-se apenas a enviar ou receber valores. Essa especifidade aumenta a segurança de tipagem do programa.

**Operador:** "<-" é usado para enviar e receber valores do channel, a "flecha" diz qual a direção da operação
**Enviar dados:** `channel <- "test"`
**Receber dados:** `chTest := <-channel`


#### Unbuffered channels

São sícronos e basicamente significa que não existe buffer, entre a goroutine remetente (sender) e a goroutine destinatario (receiver).
Assim, se não existe buffer, a goroutine "remetente" vai ficar bloqueada até que tenha uma goroutine "destinatario" ouvindo 	esse channel. O mesmo acontece se uma goroutine estiver esperando por um channel na qual não tem uma goroutine para enviar os valores.

#### Buffered/Unbuffered channels

Buffered channel são assícronos e quer dizer que existe um buffer entre a goroutine "remetente" e a goroutine "destinatario". Neles nos conseguimos definir a capacidade, na qual seria o tamanho do buffer que indica o numero de elementos que podem ser enviados sem que a goroutine "destinatario" esteja pronta para receber os valores.

![Unbuffered/Buffered](../img/CHannels-unbuffered-buffered.png)

![Buffer size](../img/channel_unbuffered_buffered-size.png)


A goroutine **sender** continua enviando informações sem ser bloqueado até que o buffer fique cheio assim, ela fia bloqueada tentando enviar informações até que alguma goroutine **receiver** leia a informação nesse channel, o bufferd tenha espaço e ela volte a mandar informações sem ser bloqueada

O mesmo acontece com uma goroutine **receiver** ficar ouvindo em um channel vazio!

#### Channels são bloqueantes

**channel <- value** A goroutine espera o um receiver estar pronto para receber o valor **<- channel** goroutine espera para que um valor seja recebido

> É de responsabilidade do channel fazer com que a goroutine esteja pronta novamente assim que tiver algum dado dentro dele.

#### Cuidado com channels

Valores padrões dos channels: `nil`

`var ch chan interface{}`

Assim, temos que alocar memória com a função **make** ao criar o channel

Read/write em um channel nulo/nil vai fazer o código bloquear para sempre 

```golang
var ch chan interface{}

<-ch

ch <- struct{}
```

Fechar um channel nulo/nil vai quebrar o código ao receber um `panic` 

```golang
var ch chan interface{}

close(ch)
```

Tenha certeza de que o channel foi inicializado primeiramente


```golang
value,ok := <-ch

/*
ok = true, valor gerado por alguma goroutine escrevendo valores
ok = false, valor gerado por um close
*/
```

> Você pode utilizar apenas `value := <- ch` também !


#### Range channel

```golang
for value := range channel {
	...
}
```

- Itera sobre valores recebidos no channel
- O loop encerra automaticamente quando o channel fecha
- Range não retorna a variavel "ok"





Exemplo 1:

```golang
// Func escreve - recebe dois parametros, mensage string e canal string, envia uma mensagem pelo canal
func escreve(mensage string, canal chan string) {
	for i := 0; i < 5; i++ {
		canal <- mensage // enviando um valor para canal
		time.Sleep(time.Second)
	}
	close(canal) // fechando o canal, uso indicado para evitar o deadlock
}

func main() {
	// criando um channel
	canal := make(chan string)

	fmt.Println("Inicio do programa.")

	go escreve("Teste K", canal)

	/* o canal tem duas operações que são eviar um dado e receber um dado, o problema aqui é elas são bloqueantes, quando esse canal
	receber a mensagem ele vai encerra a execução do problema
	*/

	// // Exemplo 1: Loop infinito, com uma condicional, caso o canal fechado encerra loop
	for {
		mensagem, aberto := <-canal // o canal está esperar receber um valor, caso o canal seja fechado ele encerra a execução
		if !aberto {
			break
		}
		fmt.Println(mensagem)
	}

	fmt.Println("Finalizando programa!")

}

```

Exemplo 2:

```golang
// Func escreve - recebe dois parametros, mensage string e canal string, envia uma mensagem pelo canal
func escreve(mensage string, canal chan string) {
	for i := 0; i < 5; i++ {
		canal <- mensage // enviando um valor para canal
		time.Sleep(time.Second)
	}
	close(canal) // fechando o canal, uso indicado para evitar o deadlock
}

func main() {
	// criando um channel
	canal := make(chan string)

	fmt.Println("Inicio do programa.")

	go escreve("Teste K", canal)

	// Exemplo 2: Loop simplificado, enguanto receber mensagem ele printa da tela, quando o canal fechar a execução é encerrada
	for mensagem := range canal {

		fmt.Println(mensagem)
	}

	fmt.Println("Finalizando programa!")
}
```

#### Canais com buffer

```golang
func main() {
	canal := make(chan string, 2) // o número é um buffer do canal, informando a capacidade que é de duas entradas, isso evita o `deadlock`

	// Podemos enviar apenas duas mensagens
	canal <- "Mensagem 1"
	canal <- "Mensagem 2"

	// Podemos receber também apenas duas mensagens
	mensage := <-canal
	mensage2 := <-canal
	
	fmt.Println(mensage)
	fmt.Println(mensage2)
}
```

> Nesse exemplo, se tentarmos enviar mais de duas mensagem vamos gerar um deadlock, o mesmo vale para o recebimento.

### Select


```golang
func main() {
	canal1, canal2 := make(chan string), make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			canal1 <- "Canal 1" // enviando mensagem para o canal 1
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			canal2 <- "Canal 2" // enviando mensagem para o canal 1
		}
	}()

	for {

		select {
		case msg1 := <-canal1: // caso canal 1 pronto para receber valor, execute a proxima linha é um print
			fmt.Println(msg1)
		case msg2 := <-canal2: // caso canal 2 pronto para receber valor, execute a proxima linha é um print
			fmt.Println(msg2)
		}
	}

}
```

### Padrão worker pools

```golang
// Função recursiva
func fibonati(posicao int) int {
	if posicao <= 1 {
		return posicao
	}

	return fibonati(posicao-2) + fibonati(posicao-1)
}

// Especificando que o canal tarefas só recebe dados `<-` e o canal resultado só recebe dados, ps a posição é importante, siga o exemplo
func worker(tarefas <-chan int, resultados chan<- int) {
	for numero := range tarefas {
		resultados <- fibonati(numero) // para cada entrada será envio o valor para o canal resultado
	}
}

func main() {
	tarefas := make(chan int, 45)
	resultados := make(chan int, 45)

	// Aumentando numeros de rotinas, para aumentar a velocidade de processamento
	go worker(tarefas, resultados)
	go worker(tarefas, resultados)
	go worker(tarefas, resultados)
	go worker(tarefas, resultados)

	for i := 0; i < 45; i++ {
		tarefas <- i
	}

	close(tarefas) // fechando canal

	for i := 0; i < 45; i++ {
		resultado := <-resultados
		fmt.Println(resultado)
	}
}
```

### Padrão generator

Abstrai a complexidade dentro de uma função.

```golang
// Retorna um canal tipo string
func escreve(texto string) <-chan string {
	canal := make(chan string)
	go func() {
		for {
			canal <- fmt.Sprintf("Valor recebido: %s", texto)
			time.Sleep(time.Millisecond * 500)
		}
	}()

	return canal
}

func main() {
	canal := escreve("Teste k")

	// Aqui eu controle a quantidade de vezes que a mensagem será impressa na tela
	for i := 0; i < 10; i++ {
		fmt.Println(<-canal)
	}
}
```

### Padrão de multiplexação

```golang
// Retorna um canal tipo string
func escreve(texto string) <-chan string {
	canal := make(chan string)
	go func() {
		for {
			canal <- fmt.Sprintf("Valor recebido: %s", texto)
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(2000)))
		}
	}()

	return canal
}

// Func que recebe dois canais e retorna um canal
func multiplexar(canal1, canal2 <-chan string) <-chan string {
	canalSaida := make(chan string)

	// recebendo entrada de ambos canais e convertendo para apenas um canal, tudo feito dentro de uma func anonima
	go func() {
		for {
			select {
			case msg := <-canal1:
				canalSaida <- msg
			case msg := <-canal2:
				canalSaida <- msg
			}
		}
	}()

	return canalSaida
}

func main() {

	canal := multiplexar(escreve("Teste k"), escreve("Programando em GO!"))
	for i := 0; i < 10; i++ {
		fmt.Println(<-canal)
	}
}
```

## Testes automatizados

Test unit básico 

Existem algumas ponto a se considerar

1. O nome do arquivo de teste deve-se ter um sufixo `<nome qualquer>_test.go`
2. Func da chamada do teste deve-ser ter um prefixo `Test<nome>`

```golang
func TestEndereco(t *testing.T) {
	enderecoTest := "Avenida Paulista"
	tipoEnderecoEsperado := "Avenida"
	tipoEnderecoRecebido := ValidarEnderecos(enderecoTest)

	if tipoEnderecoRecebido != tipoEnderecoEsperado {
		t.Error("Tipo diferente do esperado!")
	}
}
```

Test mult-cenarios

```golang
type cenarioTest struct {
	enderecoRecebido string
	enderecoEsperado string
}

func TestEndereco(t *testing.T) {
	cenario1 := []cenarioTest{
		{"Rua 123", "Rua"},
		{"Avenida paulista", "Avenida"},
		{"Rodovia paulista", "Rodovia"},
		{"Praça paulista", "Tipo inválido!"},
		{"", "Tipo inválido!"},
		{"Estrada paulista", "Estrada"},
		{"Viela paulista", "Tipo inválido!"},
	}

	for _, cenario := range cenario1 {
		tipoEndereco := ValidarEnderecos(cenario.enderecoRecebido) // Chamando a função para test
		if tipoEndereco != cenario.enderecoEsperado {
			t.Errorf("Valor recebido %q => Valor esperado %q\n", tipoEndereco, cenario.enderecoEsperado)
		}
	}
}
```

### Dicas test e relátorios de cobertura de código

Para executar tests de todos pacotes execute:

```bash
go test ./...
```
> Execute no dir raiz do projeto

Cobertura de test (%)

```bash
go test --cover
```

Cobertura de test melhorada

```bash
go test --coverprofile <nome do arquivo resultado cobertura dos tests>.txt
```

Lendo arquivo resultados dos tests de maneira mais amigavel

```bash
go tool cover --func==<arquivo>.txt
```

Agora vamos visualizar exatamente quais linhas não são testadas por nossos tests, de forma visual

```bash
go tool cover --html=<arquivo>.txt
```

Será gerado um html, mostrando a cobetura de test.

---

## Fundamentos adicionais

### Json - função marshal


Convertendo struct para json

```
type cao struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	c1 := cao{"Lili", "Vira-lata", 1}
	fmt.Println(c1)

	// Convertendo o struct em json
	// É uma boa pratica declara a variavel de conversão em json com final "JSON"
	caoEmJSON, err := json.Marshal(c1)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(caoEmJSON) // saida será um slice de bytes

	// Convertendo para um json para humanos entenderem
	fmt.Println(bytes.NewBuffer(caoEmJSON))
}
```

Convertendo map para json

```
type cao struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	fmt.Println("========")
	// Conversão em json de map
	c2 := map[string]string{
		"nome": "Tiozinho",
		"raca": "Pintcher",
	}

	cao2EmJSON, err := json.Marshal(c2)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(cao2EmJSON)                  // slice bytes
	fmt.Println(bytes.NewBuffer(cao2EmJSON)) // convertendo slice de bytes para humanos

}
```

## Unmarshal

```
type cao struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	c1JSON := `{"nome":"Lili","raca":"Vira-lata","idade":1}`

	var c1 cao

	// Json para struct
	if err := json.Unmarshal([]byte(c1JSON), &c1); err != nil {
		log.Fatal(err)
	}

	fmt.Println(c1)

	// Json para map
	c2JSON := `{"nome":"Tiozinho","raca":"Vira-lata"}`

	c2 := make(map[string]string)

	if err := json.Unmarshal([]byte(c2JSON), &c2); err != nil {
		log.Fatal(err)
	}
	fmt.Println(c2)
}
```

## Protocolo HTTP


- Requet e Response
- Rotas
	- URI - Identificador de recurso
	- Métodos - GET, Post, PUT, DELETE

Criando um servidor local

```golang
func main() {
	// Criando um servidor http
	log.Fatal(http.ListenAndServe(":5000", nil))
}
```

Criando rotas simples

```golang
// Func usuarios - exemplo 2
func usuarios(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Página de usuários!")) // Escrevendo mensagem simples na tela
}

func main() {
	// Criando um servidor http

	// Exemplo 1
	http.HandleFunc("/home", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Olá, Mundo!"))// Escrevendo mensagem simples na tela
	})

	// Exemplo 2
	http.HandleFunc("/usuarios", usuarios)

	log.Fatal(http.ListenAndServe(":5000", nil))
}

```
> Execute o programa e você pode acessar via browser na porta `:5000` `+` `/home|/usuarios`

## Banco de dados

Create database

```mysql
CREATE DATABASE devbook;
```

Usando database

```mysql
USE devbook;
```

Criando tabelas

```mysql
CREATE TABLE usuarios(
	id int auto_increment primary key,
	nome varchar(50) not null,
	email varchar(50) not null
) ENGINE=INNODB;
```

Create user go

```mysql
CREATE USER 'golang'@'%'IDENTIFIED BY 'golang';
// CREATE USER 'golang'@'localhost'IDENTIFIED BY 'golang';

GRANT ALL PRIVILEGES on devbook.* TO 'golang'@'%';
// GRANT ALL PRIVILEGES on devbook.* TO 'golang'@'localhost';

FLUSH PRIVILEGES;
```

```mysql
SELECT User, Host, Password FROM mysql.user; // Select users
DROP USER 'golang'@'localhost'; // delete user
```


Criando conexão com banco de dados

```bash
go get github.com/go-sql-driver/mysql
```

---
# Referencias

- [Aprenda golang do zero desenvolva uma aplicacao completa](https://www.udemy.com/course/aprenda-golang-do-zero-desenvolva-uma-aplicacao-completa/)
- [Material de apoio - modulo 1 e 2](https://github.com/OtavioGallego/curso-golang)
- [Material de apoio - aplicação](https://github.com/OtavioGallego/devbook)
- [mermaid - journey](https://mermaid-js.github.io/mermaid/#/user-journey)
- [Externo pacotes - checkmail](https://github.com/badoux/checkmail)
- [VSCode auto import problem solved for Golang | Dr Vipin Classes](https://www.youtube.com/watch?v=R2sbZM8WTWE)
- [Golang goroutines](https://gobyexample.com/goroutines)
- [Goroutines e go channels](https://medium.com/trainingcenter/goroutines-e-go-channels-f019784d6855)
- [Waitgroup](https://gobyexample.com/waitgroups)
- [Usando waitgroup em golang](https://acervolima.com/usando-waitgroup-em-golang/)
- [Round](https://yourbasic.org/golang/round-float-2-decimal-places/)
- [GoLang Assíncrono - Como funcionam os channels em Go](https://www.youtube.com/watch?v=pKMkf-Dp3Is&t=2s)
- [golang tour - channels](https://go.dev/tour/concurrency/2)

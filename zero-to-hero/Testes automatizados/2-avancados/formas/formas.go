package formas

import (
	"fmt"
	"math"
)

// Interface - receber uma assinatura de um metodo, que deve corresponder ao que foi declarado
type Forma interface {
	Area() float64
}

func EscreverArea(f Forma) {
	fmt.Printf("A àrea é %0.2f\n", f.Area())
}

type Retangulo struct {
	Altura  float64
	Largura float64
}

type Circulo struct {
	Raio float64
}

func (c Circulo) Area() float64 {
	return math.Pi * (c.Raio * c.Raio)
}

func (r Retangulo) Area() float64 {
	return r.Altura * r.Largura
}

package enderecos

import "strings"

// ValidarEnderecos - Valida tipo de endereco valido e o retorna
func ValidarEnderecos(endereco string) string {
	tiposvalidos := []string{"rua", "avenida", "estrada", "rodovia"}
	padronizaEndereco := strings.ToLower(endereco)
	primeiraPalavra := strings.Split(padronizaEndereco, " ")[0]

	tipoValido := false

	for _, tipo := range tiposvalidos {
		if tipo == primeiraPalavra {
			tipoValido = true
		}
	}

	if tipoValido {
		return strings.Title(primeiraPalavra)
	}

	return "Tipo inválido!"
}

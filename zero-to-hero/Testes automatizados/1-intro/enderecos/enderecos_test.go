package enderecos_test

import (
	. "intro-testes/enderecos" // Ao utilizar o _test como pack, para que possamos chamar todas func como locais, add o "." como alias
	"testing"
)

type cenarioTest struct {
	enderecoRecebido string
	enderecoEsperado string
}

func TestEndereco(t *testing.T) {
	t.Parallel()
	cenario1 := []cenarioTest{
		{"Rua 123", "Rua"},
		{"Avenida paulista", "Avenida"},
		{"Rodovia paulista", "Rodovia"},
		// {"Praça paulista", "Tipo inválido!"},
		// {"", "Tipo inválido!"},
		{"Estrada paulista", "Estrada"},
		// {"Viela paulista", "Tipo inválido!"},
	}

	for _, cenario := range cenario1 {
		tipoEndereco := ValidarEnderecos(cenario.enderecoRecebido) // Chamando a função para test
		if tipoEndereco != cenario.enderecoEsperado {
			t.Errorf("Valor recebido %q => Valor esperado %q\n", tipoEndereco, cenario.enderecoEsperado)
		}
	}
}

func TestQualquer(t *testing.T) {
	t.Parallel()
	if 1 > 2 {
		t.Errorf("Este falhou")
	}
}

package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var templates *template.Template

type usuario struct {
	Nome  string `json:"nome"`
	Email string `json:"email"`
}

func main() {

	templates = template.Must(templates.ParseGlob("*.html"))

	// Rota home
	http.HandleFunc("/home", func(w http.ResponseWriter, r *http.Request) {
		user := usuario{"Jinx", "jinx@gmail.com"}
		templates.ExecuteTemplate(w, "home.html", user)
	})

	fmt.Println("Escutando na porta 5000...")
	log.Fatal(http.ListenAndServe(":5000", nil))
}

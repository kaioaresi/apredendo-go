package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type cao struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	c1JSON := `{"nome":"Lili","raca":"Vira-lata","idade":1}`

	var c1 cao

	// Json para struc
	if err := json.Unmarshal([]byte(c1JSON), &c1); err != nil {
		log.Fatal(err)
	}

	fmt.Println(c1)

	// Json para map
	c2JSON := `{"nome":"Tiozinho","raca":"Vira-lata"}`

	c2 := make(map[string]string)

	if err := json.Unmarshal([]byte(c2JSON), &c2); err != nil {
		log.Fatal(err)
	}
	fmt.Println(c2)
}

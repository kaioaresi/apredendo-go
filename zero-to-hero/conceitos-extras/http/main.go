package main

import (
	"log"
	"net/http"
)

func barra(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Rota default"))
}

func home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Olá, Mundo!"))
}

func usuarios(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Página de usuários!"))
}

func main() {

	// Rota raiz
	http.HandleFunc("/", barra)

	// Rota home
	http.HandleFunc("/home", home)

	// Rota usuários
	http.HandleFunc("/usuarios", usuarios)

	log.Fatal(http.ListenAndServe(":5000", nil))
}

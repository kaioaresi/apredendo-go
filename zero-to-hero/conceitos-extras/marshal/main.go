package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
)

type cao struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	c1 := cao{"Lili", "Vira-lata", 1}
	fmt.Println(c1)

	// Convertendo o struct em json
	// É uma boa pratica declara a variavel de conversão em json com final "JSON"
	caoEmJSON, err := json.Marshal(c1)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(caoEmJSON) // saida será um slice de bytes

	// Convertendo para um json para humanos entenderem
	fmt.Println(bytes.NewBuffer(caoEmJSON))

	fmt.Println("========")
	// Conversão em json de map
	c2 := map[string]string{
		"nome": "Tiozinho",
		"raca": "Pintcher",
	}

	cao2EmJSON, err := json.Marshal(c2)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(cao2EmJSON)                  // slice bytes
	fmt.Println(bytes.NewBuffer(cao2EmJSON)) // convertendo slice de bytes para humanos

}

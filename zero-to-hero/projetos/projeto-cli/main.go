package main

import (
	"fmt"
	"log"
	"os"
	"projeto-cli/app"
)

func main() {
	fmt.Println("Projeto CLI")
	aplicacao := app.Gerar()
	if err := aplicacao.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

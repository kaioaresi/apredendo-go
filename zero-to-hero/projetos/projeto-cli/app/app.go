package app

import (
	"fmt"
	"log"
	"net"

	"github.com/urfave/cli"
)

// Gerar vai retornar a aplicação de linha de comando pronta para ser executada
func Gerar() *cli.App {
	app := cli.NewApp()
	app.Name = "App de linha de comando"
	app.Usage = "Busca IPs e Nomes de servidor na internet"
	flags := []cli.Flag{
		cli.StringFlag{
			Name:  "host",
			Value: "google.com",
		},
	}
	app.Commands = []cli.Command{
		{
			Name:   "ip",
			Usage:  "Busca ips na net e retorna a lista de IPs!",
			Flags:  flags,
			Action: buscarIPs,
		},
		{
			Name:   "servidores",
			Usage:  "Buscar servidores!",
			Flags:  flags,
			Action: buscarServidores,
		},
	}

	return app
}

// Func que busca IPs - (projeto-cli ip --host <URL>)
func buscarIPs(c *cli.Context) {
	host := c.String("host")
	ips, err := net.LookupIP(host)

	if err != nil {
		log.Fatal(err)
	}

	for _, ip := range ips {
		fmt.Println(ip)
	}
}

// Func busca servidores - (projeto-cli servidores --host <URL>)
func buscarServidores(c *cli.Context) {
	host := c.String("host")
	servidores, err := net.LookupNS(host) // NS == name server

	if err != nil {
		log.Fatal(err)
	}

	for _, servidor := range servidores {
		fmt.Println(servidor.Host)
	}
}
